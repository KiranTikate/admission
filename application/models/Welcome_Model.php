 
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	class Welcome_Model extends CI_Model
	{



       public function submit_student($student_data){
            //  print_r($student_data);
            // print_r($student_data['ph_no_3']);exit();
            $phone=$student_data['ph_no_3'];
            $sql="SELECT * FROM student_form  where ph_no_3=$phone";
            $query=$this->db->query($sql);
            // print_r($sql);
                $result = $query->result_array();


                //  print_r($result); exit();

                if($result==null){
                	// $id=0;
                		$q=$this->db->insert('student_form',$student_data);
                	// print_r($id);
                }else{
                	 $id=$result[0]['st_id'];

                	 $this->db->where('st_id', $id);
		$q=$this->db->update('student_form',$student_data);

                }
            //   print_r($q);exit();
            return $q;
        }

		public function form_request_list(){
			$q="SELECT student_form.*,class.* From student_form INNER JOIN class ON class.id=student_form.class  where student_form.status=1 ORDER BY student_form.created_on ASC";
			$sql=$this->db->query($q);
			$result=$sql->result_array();
			return $result;
		}

		public function approve_student(){
		
			$this->db->set('status', 3);
			$this->db->where('st_id', $this->input->post('st_id'));
			$q=$this->db->update('student_form');
			return $q;
		
		}
		public function delete_student(){
		
			$this->db->set('status', 2);
			$this->db->where('st_id', $this->input->post('st_id'));
			$q=$this->db->update('student_form');
			return $q;
		
		}
		
      public function form_approve_list(){
			$q="SELECT student_form.*,class.* From student_form INNER JOIN class ON class.id=student_form.class  where student_form.status=3";
			$sql=$this->db->query($q);
			$result=$sql->result_array();
			return $result;
		}


		public function back_student(){
		
			$this->db->set('status', 1);
			$this->db->where('st_id', $this->input->post('st_id'));
			$q=$this->db->update('student_form');
			return $q;
		
		}

		public function form_reject_list(){
			$q="SELECT student_form.*,class.* From student_form INNER JOIN class ON class.id=student_form.class  where student_form.status=2";
			$sql=$this->db->query($q);
			$result=$sql->result_array();
			return $result;
		}

	public function send_mail(){
		$st_id=$this->input->post('st_id');
		$q="SELECT * From student_form where st_id=$st_id";
		$sql=$this->db->query($q);
		$result=$sql->row();
		return $result;
		


    }			
	
    
    public function user_password($ph_no,$pass,$st_id){
        $data = array(
					'user_type_id'=>2,	
					  'st_id'=>$st_id,
					  'phone_no'=>$ph_no,
					  'password'=>$pass,
       				 

						);
		$q=$this->db->insert('admin', $data);
    }


    public function student_request_list(){
    	$sql="SELECT COUNT(*) as st_id FROM `student_form` WHERE status=1";
    	
		$query=$this->db->query($sql);
		return $query->row();
		
    }

     public function student_reject_list(){
    	$sql="SELECT COUNT(*) as st_id FROM `student_form` WHERE status=2";
    	
		$query=$this->db->query($sql);
		return $query->row();
		
    }

     public function student_approve_list(){
    	$sql="SELECT COUNT(*) as st_id FROM `student_form` WHERE status=3";
    	
		$query=$this->db->query($sql);
		return $query->row();
		
    }




    public function add_class(){
    	$data=$this->input->post();
            $q=$this->db->insert('class',$data);
            return $q;
        }
    
   public function user_details($user_id){
    	$sql="SELECT student_form.*,class.* From student_form INNER JOIN class ON class.id=student_form.class where st_id=$user_id";
    	$query=$this->db->query($sql);
    	return $query->row();
    }

    public function class_list(){
    	$sql="SELECT * From class where status_id=1 ORDER BY class ASC";
    	$query=$this->db->query($sql);
    	return $query->result_array();
    }



    public function add_fee_category()
		{
			$data=$this->input->post();
			$q=$this->db->insert('fee_category',$data);
			return $q;
		}

		public function get_fee_category()
		{
			$this->db->select('*');
			$this->db->where('status_id', 1); 
			$q=$this->db->get('fee_category');
			return $q->result_array();
		}

    public function edit_fee_category()
		{
			$data = $this->input->post();

			$this->db->where('fee_category_id', $this->input->post('fee_category_id'));
			$q=$this->db->update('fee_category', $data);
			return $q;
		}


		public function delete_fee_category_ajax()
		{
			$this->db->set('status_id', 2);
			$this->db->where('fee_category_id', $this->input->post('fee_category_id'));
			$q=$this->db->update('fee_category');
			return $q;
		}

		public function get_classes()
		{
			$this->db->select('*');
			$this->db->where('status_id', 1); 
			$q=$this->db->get('class');
			return $q->result_array();
		}

			public function get_all_categories()
		{
			$this->db->select('*');
			 $this->db->from('fee_category');
			 // $this->db->join('academic_year','academic_year.academic_year_id=exam.academic_year_id');
			$this->db->where('fee_category_status',1);
			$this->db->where('status_id',1);
			 $query=$this->db->get();
			 return $query->result_array();

		}

 	public function delete_class($id){
 		$this->db->set('status_id', 2);
			$this->db->where('id', $id);
			$q=$this->db->update('class');
			return $q;
 	}
		


public function add_fees_amount_form()
    {
      $data=$this->input->post(); 
      $q=$this->db->insert('fee_category_amount',$data);
      return $q;
    }

       public function get_fee_amount()
    {
//      $this->db->select('*');
//      $this->db->where('status_id', 1);
//      $q=$this->db->get('fee_category_amount');
//      return $q->result_array();
      
       $this->db->select('fee_category_amount.*,class.*,fee_category.fee_category_name');
       $this->db->from('fee_category_amount');
       $this->db->join('fee_category','fee_category.fee_category_id=fee_category_amount.fee_category_id');
        $this->db->join('class','class.id=fee_category_amount.id');
       $this->db->where('fee_category_amount.status_id',1);
        $this->db->where('fee_category.status_id',1);
       $query=$this->db->get();
      return $data=$query->result_array(); 
      
    }
    // end of Display fees Amount

    // edit fees amount

    public function edit_fee_amount_form()
    {
      $data = $this->input->post();

      $this->db->where('fee_category_amount_id', $this->input->post('fee_category_amount_id'));
      $q=$this->db->update('fee_category_amount', $data);
      return $q;
    }
    // end fees amount

    //delete fees amount

    public function delete_fee_amount_form()
    {
      $this->db->set('status_id', 2);
      $this->db->where('fee_category_amount_id', $this->input->post('fee_category_amount_id'));
      $q=$this->db->update('fee_category_amount');
      return $q;
    }
    
    
    		public function get_fee_collect_data($id)
		{


	
			 	$sql="SELECT class.id,class.class, fee_category_amount.fee_category_id, fee_category.fee_category_id, fee_category.fee_category_name, fee_category_amount.fee_sub_category_name, fee_category_amount.fee_category_amount_id, fee_category_amount.fee_amount FROM fee_category_amount INNER JOIN fee_category ON fee_category.fee_category_id=fee_category_amount.fee_category_id INNER JOIN class ON class.id=fee_category_amount.id WHERE fee_category_amount.id=$id GROUP BY fee_category_amount.fee_category_amount_id";
                                                 
                                                    
                                                    $q=$this->db->query($sql);
                                                    $result=$q->result_array();
                                                    return $result;
		// }

}

public function fees_structure($class)
		{


	
			 	$sql="SELECT class.id,class.class, fee_category_amount.fee_category_id, fee_category.fee_category_id, fee_category.fee_category_name, fee_category_amount.fee_sub_category_name, fee_category_amount.fee_category_amount_id, fee_category_amount.fee_amount FROM fee_category_amount INNER JOIN fee_category ON fee_category.fee_category_id=fee_category_amount.fee_category_id INNER JOIN class ON class.id=fee_category_amount.id WHERE fee_category_amount.id=$class GROUP BY fee_category_amount.fee_category_amount_id";
                                                 
                                                    
                                                    $q=$this->db->query($sql);
                                                    $result=$q->result_array();
                                                    return $result;
		// }

}

public function fees_structure1($class)
		{


	
			 	$sql="SELECT class.id,class.class, fee_category_amount.fee_category_id, fee_category.fee_category_id, fee_category.fee_category_name, fee_category_amount.fee_sub_category_name, fee_category_amount.fee_category_amount_id, fee_category_amount.fee_amount FROM fee_category_amount INNER JOIN fee_category ON fee_category.fee_category_id=fee_category_amount.fee_category_id INNER JOIN class ON class.id=fee_category_amount.id WHERE fee_category_amount.id=$class GROUP BY fee_category_amount.fee_category_amount_id";
                                                 
                                                    
                                                    $q=$this->db->query($sql);
                                                    $result=$q->result_array();
                                                    return $result;
		// }

}

public function student_class(){
	$class=$this->input->post('class');
 	$sql="SELECT * From class where id=$class";
    	$query=$this->db->query($sql);
    	return $query->row();
}

public function fees_payment($class,$fee_category_amount_id)
		{


	
			 	$sql="SELECT class.id,class.class, fee_category_amount.fee_category_id, fee_category.fee_category_id, fee_category.fee_category_name, fee_category_amount.fee_sub_category_name, fee_category_amount.fee_category_amount_id, fee_category_amount.fee_amount FROM fee_category_amount INNER JOIN fee_category ON fee_category.fee_category_id=fee_category_amount.fee_category_id INNER JOIN class ON class.id=fee_category_amount.id WHERE fee_category_amount.id=$class AND fee_category_amount.fee_category_amount_id=$fee_category_amount_id GROUP BY fee_category_amount.fee_category_amount_id";
                                                 
                                                    
                                                    $q=$this->db->query($sql);
                                                    $result=$q->row();
                                                    return $result;
		// }

}

public function fees_status($user_id){

	$sql="SELECT payment.*,payment_status.* FROM `payment_status` JOIN payment ON payment.customerId = payment_status.customerId WHERE payment_status.customerId=$user_id";
	$q=$this->db->query($sql);
    $result=$q->result_array();

    return $result;

	// $data['query']=$query->result_array();

}


public function payment_status(){
	// $data1=$this->input->post();

	$data['orderId']=$this->input->post('orderId');
	$data['fee_id']=$this->input->post('fee_id');
	$data['customerId']=$this->input->post('customerId');

	$customer_Id=$data['customerId'];
	$feeid=$data['fee_id'];

	$sql="SELECT * FROM payment_status where customerId=$customer_Id AND fee_id=$feeid";
	$q=$this->db->query($sql);
	$result=$q->row();

if($result==null){

	 $q=$this->db->insert('payment_status',$data);
	} else{
		$this->db->where('customerId', $customer_Id);
		$this->db->where('fee_id', $feeid);
		$q=$this->db->update('payment_status',$data);
	}

      return $q;
}

}

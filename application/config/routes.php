<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
// $route['form']= 'admin/online_admission';
$route['form']='Welcome/form';
$route['submit_student']='Welcome/submit_student';

 
$route['login']='Login';
$route['sign_in']='Login/sign_in';

$route['userlogin']='User_login';
$route['user_sign_in']='User_login/user_sign_in';

$route['form_request_list']='admin/form_request_list';
$route['form_conform_list']='admin/form_conform_list';
$route['form_approve_list']='admin/form_approve_list';
$route['form_reject_list']='admin/form_reject_list';
$route['approve_student']='admin/approve_student';
$route['delete_student']='admin/delete_student';
$route['back_student']='admin/back_student';
$route['class']='admin/class';
$route['add_class']='admin/add_class';
$route['fees']='admin/fees';

$route['add_fee_category']='admin/add_fee_category';
$route['edit_fee_category']='admin/edit_fee_category';

$route['get_fee_category']='admin/get_fee_category';


$route['delete_fee_category_ajax']='admin/delete_fee_category_ajax';

$route['get_all_categories']='admin/get_all_categories';
$route['add_fees_amount_form_kiran']='admin/add_fees_amount_form_kiran';
$route['get_fee_amount']='admin/get_fee_amount';
$route['edit_fee_amount_form']='admin/edit_fee_amount_form';
$route['delete_fee_amount_form']='admin/delete_fee_amount_form';


$route['get_fee_collect_data/(:num)']='admin/get_fee_collect_data/$1';
// $route['delete_class/(:num)']='admin/delete_class/$1';

$route['delete_class/(:num)']='admin/delete_class/$1';


$route['admin']='admin';

$route['user']='user';

$route['payment']='user/payment';

$route['logout']='Welcome/logout';

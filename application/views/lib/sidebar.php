
<!-- Start wrapper-->
 <div id="wrapper">

    <div id="sidebar-wrapper" class="bg-theme bg-theme2" data-simplebar="" data-simplebar-auto-hide="true">
    


    <?php $my_session=$this->session->userdata('user_type_id');

if($my_session == "1")
        {

         ?>

 <div class="brand-logo">
      <a href="<?php echo  base_url('admin');?>">
       <a href="#intro" class="scrollto"><img src="<?php echo base_url('') ?>home/img/wklogo.png" style="width: 40px;"  alt="" class="img-fluid"></a>
       <h5 class="logo-text"> Admin</h5>
     </a>
   </div>
   <div class="user-details">
    <div class="media align-items-center user-pointer collapsed" data-toggle="collapse" data-target="#user-dropdown">
      <div class="avatar"><a href="#intro" class="scrollto"><img src="<?php echo base_url('') ?>home/img/wklogo.png"  alt="" class="img-fluid"></a></div>
       <div class="media-body">
       <h6 class="side-user-name">Tiranga</h6>
      </div>
       </div>
     <div id="user-dropdown" class="collapse">
      <ul class="user-setting-menu">
            <li><a href="javaScript:void();"><i class="icon-user"></i>  My Profile</a></li>
            <!-- <li><a href="javaScript:void();"><i class="icon-settings"></i> Setting</a></li> -->
      <li><a href="<?php echo base_url('logout');?>">LogOut</a></li>
      </ul>
     </div>
      </div>
   <ul class="sidebar-menu do-nicescrol">
      <li class="sidebar-header">MAIN NAVIGATION</li>
      <li>
        <a href="<?php echo base_url('admin'); ?>" class="waves-effect">
          <i class="zmdi zmdi-view-dashboard"></i> <span>Dashboard</span><i class="fa fa-angle-left pull-right"></i>
        </a>
  <!--  <ul class="sidebar-submenu">
      <li><a href="index.html"><i class="zmdi zmdi-long-arrow-right"></i> Ecommerce</a></li>
          <li><a href="index2.html"><i class="zmdi zmdi-long-arrow-right"></i> Property Listings</a></li>
      <li><a href="dashboard-service-support.html"><i class="zmdi zmdi-long-arrow-right"></i> Services & Support</a></li>
      <li><a href="dashboard-logistics.html"><i class="zmdi zmdi-long-arrow-right"></i> Logistics</a></li>
    </ul> -->
      </li>
    <!--   <li>
        <a href="javaScript:void();" class="waves-effect">
          <i class="zmdi zmdi-layers"></i>
          <span>UI Elements</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
    <li><a href="ui-typography.html"><i class="zmdi zmdi-long-arrow-right"></i> Typography</a></li>
        <li><a href="ui-cards.html"><i class="zmdi zmdi-long-arrow-right"></i> Cards</a></li>
    <li><a href="ui-buttons.html"><i class="zmdi zmdi-long-arrow-right"></i> Buttons</a></li>
        <li><a href="ui-nav-tabs.html"><i class="zmdi zmdi-long-arrow-right"></i> Nav Tabs</a></li>
        <li><a href="ui-accordions.html"><i class="zmdi zmdi-long-arrow-right"></i> Accordions</a></li>
        <li><a href="ui-modals.html"><i class="zmdi zmdi-long-arrow-right"></i> Modals</a></li>
        <li><a href="ui-list-groups.html"><i class="zmdi zmdi-long-arrow-right"></i> List Groups</a></li>
        <li><a href="ui-bootstrap-elements.html"><i class="zmdi zmdi-long-arrow-right"></i> BS Elements</a></li>
        <li><a href="ui-pagination.html"><i class="zmdi zmdi-long-arrow-right"></i> Pagination</a></li>
        <li><a href="ui-alerts.html"><i class="zmdi zmdi-long-arrow-right"></i> Alerts</a></li>
        <li><a href="ui-progressbars.html"><i class="zmdi zmdi-long-arrow-right"></i> Progress Bars</a></li>
    <li><a href="ui-checkbox-radio.html"><i class="zmdi zmdi-long-arrow-right"></i> Checkboxes & Radios</a></li>
        <li><a href="ui-notification.html"><i class="zmdi zmdi-long-arrow-right"></i> Notifications</a></li>
        <li><a href="ui-sweet-alert.html"><i class="zmdi zmdi-long-arrow-right"></i> Sweet Alerts</a></li>
        </ul>
      </li> -->
      
      

       <li><a href="<?php echo base_url('class'); ?>" class="waves-effect"><i class="zmdi zmdi-long-arrow-right"></i> <span>Class</span></a></li>

       <li><a href="<?php echo base_url('fees'); ?>" class="waves-effect"><i class="zmdi zmdi-card-travel"></i> <span>Fees</span></a></li>

    <li><a href="<?php echo base_url('form_request_list'); ?>" class="waves-effect"><i class="zmdi zmdi-chart-donut text-success"></i> <span>Student Admission List</span></a></li>

    <!-- <li><a href="<?php echo base_url('form_conform_list'); ?>" class="waves-effect"><i class="zmdi zmdi-chart-donut text-success"></i> <span>Conform</span></a></li> -->

    <li><a href="<?php echo base_url('form_approve_list'); ?>" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span>Student Approved List</span></a></li>

    <li><a href="<?php echo base_url('form_reject_list'); ?>" class="waves-effect"><i class="zmdi zmdi-layers"></i> <span>Student Rejected List</span></a></li>
      

    
     
    

      <!-- <li><a href="javaScript:void();" class="waves-effect"><i class="zmdi zmdi-chart-donut text-success"></i> <span>Warning</span></a></li> -->
      <li><a href="javaScript:void();" class="waves-effect"><i class="zmdi zmdi-share text-info"></i> <span>Information</span></a></li>
    </ul>
   

  <?php } else if($my_session == "2"){ ?>

 <div class="brand-logo">
      <a href="<?php echo  base_url('user');?>">
      <a href="#intro" class="scrollto"><img src="<?php echo base_url('') ?>home/img/wklogo.png" style="width: 40px" alt="" class="img-fluid"></a>
       <h5 class="logo-text"> Student Profile</h5>
     </a>
   </div>
<div class="user-details">
    <div class="media align-items-center user-pointer collapsed" data-toggle="collapse" data-target="#user-dropdown">
      <div class="avatar"><a href="#intro" class="scrollto"><img src="<?php echo base_url('') ?>home/img/wklogo.png"  alt="" class="img-fluid"></a></div>
       <div class="media-body">
       <h6 class="side-user-name"><?php echo $q->fname ; ?></h6>
      </div>
       </div>
     <div id="user-dropdown" class="collapse">
      <ul class="user-setting-menu">
            <li><a href="javaScript:void();"><i class="icon-user"></i>  My Profile</a></li>
            <li><a href="javaScript:void();"><i class="icon-settings"></i> Setting</a></li>
      <li><a href="<?php echo base_url('logout');?>">LogOut</a></li>
      </ul>
     </div>
      </div>
   <ul class="sidebar-menu do-nicescrol">
      <li class="sidebar-header">MAIN NAVIGATION</li>
      <li>
        <a href="<?php echo base_url('admin'); ?>" class="waves-effect">
          <i class="zmdi zmdi-view-dashboard"></i> <span>Dashboard</span><i class="fa fa-angle-left pull-right"></i>
        </a>
  <!--  <ul class="sidebar-submenu">
      <li><a href="index.html"><i class="zmdi zmdi-long-arrow-right"></i> Ecommerce</a></li>
          <li><a href="index2.html"><i class="zmdi zmdi-long-arrow-right"></i> Property Listings</a></li>
      <li><a href="dashboard-service-support.html"><i class="zmdi zmdi-long-arrow-right"></i> Services & Support</a></li>
      <li><a href="dashboard-logistics.html"><i class="zmdi zmdi-long-arrow-right"></i> Logistics</a></li>
    </ul> -->
      </li>
    <!--   <li>
        <a href="javaScript:void();" class="waves-effect">
          <i class="zmdi zmdi-layers"></i>
          <span>UI Elements</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
    <li><a href="ui-typography.html"><i class="zmdi zmdi-long-arrow-right"></i> Typography</a></li>
        <li><a href="ui-cards.html"><i class="zmdi zmdi-long-arrow-right"></i> Cards</a></li>
    <li><a href="ui-buttons.html"><i class="zmdi zmdi-long-arrow-right"></i> Buttons</a></li>
        <li><a href="ui-nav-tabs.html"><i class="zmdi zmdi-long-arrow-right"></i> Nav Tabs</a></li>
        <li><a href="ui-accordions.html"><i class="zmdi zmdi-long-arrow-right"></i> Accordions</a></li>
        <li><a href="ui-modals.html"><i class="zmdi zmdi-long-arrow-right"></i> Modals</a></li>
        <li><a href="ui-list-groups.html"><i class="zmdi zmdi-long-arrow-right"></i> List Groups</a></li>
        <li><a href="ui-bootstrap-elements.html"><i class="zmdi zmdi-long-arrow-right"></i> BS Elements</a></li>
        <li><a href="ui-pagination.html"><i class="zmdi zmdi-long-arrow-right"></i> Pagination</a></li>
        <li><a href="ui-alerts.html"><i class="zmdi zmdi-long-arrow-right"></i> Alerts</a></li>
        <li><a href="ui-progressbars.html"><i class="zmdi zmdi-long-arrow-right"></i> Progress Bars</a></li>
    <li><a href="ui-checkbox-radio.html"><i class="zmdi zmdi-long-arrow-right"></i> Checkboxes & Radios</a></li>
        <li><a href="ui-notification.html"><i class="zmdi zmdi-long-arrow-right"></i> Notifications</a></li>
        <li><a href="ui-sweet-alert.html"><i class="zmdi zmdi-long-arrow-right"></i> Sweet Alerts</a></li>
        </ul>
      </li> -->
      
    <li><a href="<?php echo base_url('user'); ?>" class="waves-effect"><i class="zmdi zmdi-chart-donut text-success"></i> <span>Payment</span></a></li>

    
      

    
     
    

      <!-- <li><a href="javaScript:void();" class="waves-effect"><i class="zmdi zmdi-chart-donut text-success"></i> <span>Warning</span></a></li> -->
      <li><a href="javaScript:void();" class="waves-effect"><i class="zmdi zmdi-share text-info"></i> <span>Information</span></a></li>
    </ul>

  <?php } ?>


   </div>
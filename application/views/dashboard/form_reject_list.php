<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codervent.com/bulona/demo/table-data-tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Jan 2019 10:14:21 GMT -->


<!-- start loader -->
   <div id="pageloader-overlay" class="visible incoming"><div class="loader-wrapper-outer"><div class="loader-wrapper-inner" ><div class="loader"></div></div></div></div>
   <!-- end loader -->

<!-- Start wrapper-->
 <div id="wrapper">
      <?php $this->load->view('lib/header.php'); ?>

  <!-- ============================================================== -->
        <?php $this->load->view('lib/sidebar.php'); ?>
   <!--End sidebar-wrapper-->

<!--Start topbar header-->

<!--End topbar header-->

<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
   
    <!-- End Breadcrumb-->
     

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <h1 align="center">Admission Reject</h1>
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No.</th>
                         <th> Student Name</th>
                        <th>Class</th>
                        <th>Gender</th>
                        <th>Birth Date</th>
                        <th>Phone Number</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                     <?php $i=1; foreach ($approve as $approve) { ?>
                  
                    <tr>
                        <td> <?php echo $i; ?></td>
                        <td><?php echo $approve['fname'] ?></td>
                        <td><?php echo $approve['class'] ?></td>
                        <td><?php echo $approve['gender'] ?></td>
                        <td><?php echo $approve['birth_date'] ?></td>
                        <td><?php echo $approve['ph_no_3'] ?></td>
                        <td>
                            <span class="badge"> 
                            <button type="button" class="btn waves-effect waves-light btn-sm btn-warning" data-toggle="modal" data-target="#exampleModal13<?php echo $approve['st_id']; ?>" data-whatever="@fat" data-toggle="tooltip" title="View" ><i class="fa fa-eye" aria-hidden="true"></i>
                                </button>

 <div class="modal fade" id="exampleModal13<?php echo $approve['st_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content details">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel13">Student Profile</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body ">
                                              <center><img src="https://hostingspell.com/assets/images/user.png" width="100px" ></center>
                                              <h4 class="text-center"></h4> 
                                              <div class="row text-center">
                                                <div class="col-md-12">
                                                                                                            <ul class="list-group" >
   <lable>Student Profile</lable>
    <lable>Student Profile</lable>
    <li class="list-group-item">Child Name :<?php echo $approve['fname']; ?> </li>
  
    <li class="list-group-item">Gender : <?php echo $approve['gender']; ?></li>
    <li class="list-group-item">Class Of Admission :<?php echo $approve['class']; ?></li>
    <li class="list-group-item">Date Of Birth :<?php echo $approve['birth_date']; ?> </li>
    <li class="list-group-item">Birth Place:<?php echo $approve['birth_place']; ?> </li>
    
   
    
   
    <lable><b>Parent Profile</b></lable>
    <li class="list-group-item">Mother Name:<?php echo $approve['mother_name']; ?> </li>
   
    
    <li class="list-group-item">Father Name: <?php echo $approve['father_name']; ?> </li>
    <li class="list-group-item">Father Email :<?php echo $approve['f_email']; ?></li>
    <li class="list-group-item">Father Occupation: <?php echo $approve['f_occup']; ?></li>
        
      <li class="list-group-item">Father Qualification :<?php echo $approve['f_quali']; ?> </li>
    <li class="list-group-item">Father Annual Income:<?php echo $approve['f_annual_income']; ?> </li>
    <lable><b>COMMUNICATION DETAILS</b></lable>
    <li class="list-group-item">ADDRESS:<?php echo $approve['address']; ?> </li>
    <li class="list-group-item">Land Mark : <?php echo $approve['land_mark']; ?></li>
    <li class="list-group-item">City :<?php echo $approve['city']; ?></li>
    <li class="list-group-item">District :<?php echo $approve['district']; ?> </li>
    <li class="list-group-item">Pin Code<?php echo $approve['pin_code']; ?> </li>
    
    <li class="list-group-item">State: <?php echo $approve['state']; ?> </li>
    <li class="list-group-item">Mobile/Whatsapp Personal No :<?php echo $approve['ph_no_3']; ?></li>
    <li class="list-group-item">Parent Number: <?php echo $approve['ph_no_4']; ?></li>
   
    <li class="list-group-item">10th School Name :<?php echo $approve['school_name']; ?> </li>
    <li class="list-group-item">10th Board<?php echo $approve['medium_inst']; ?> </li>
    <li class="list-group-item">12th School Name :<?php echo $approve['class_comp']; ?> </li>
    <li class="list-group-item">12th Board<?php echo $approve['medium_inst1']; ?> </li>
    
    
       <li class="list-group-item">Mother Tongue Of Student :<?php echo $approve['mother_tongue']; ?></li>
    <li class="list-group-item">Admission Fee Paid: <?php echo $approve['admission_fee']; ?></li>
     <li class="list-group-item">Reference :<?php echo $approve['reference']; ?></li>
    <li class="list-group-item">Select Category: <?php echo $approve['student_type']; ?></li>
    
  <li class="list-group-item">Child photo:<a href="<?php echo base_url('upload_images/student_profile_pics/').$approve['photo']; ?>" download>  <?php echo $approve['photo']; ?></a>

     </li>

     
     
 <li class="list-group-item">Aadhaar Card:<a href="<?php echo base_url('upload_images/student_aadhar/').$approve['aadhar_cer']; ?>" download>  <?php echo $approve['aadhar_cer']; ?></a>

     </li>
     
     
 <li class="list-group-item">Birth Certificate:<a href="<?php echo base_url('upload_images/student_birth/').$approve['birth_cer']; ?>" download>  <?php echo $approve['birth_cer']; ?></a>

     </li>
     
     
 <li class="list-group-item">Result of Previous School:<a href="<?php echo base_url('upload_images/student_result/').$approve['result_cer']; ?>" download>  <?php echo $approve['result_cer']; ?></a>

     </li>
   
</ul>
                  
                                                </div>
                                            
                                              </div>
                                            </div>
                                           <div class="modal-footer">
                                                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                                            </span>

                                                          
  <span class="badge">
                                                             <button type="button" class="btn waves-effect waves-light btn-sm btn-warning" data-toggle="modal" data-target="#exampleModal11<?php echo $approve['st_id']; ?>" data-whatever="@fat" data-toggle="tooltip" title="Consider" ><i class="fa fa-reply" aria-hidden="true"></i>
                                </button>

                                  <div class="modal fade" id="exampleModal11<?php echo $approve['st_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel11">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                             <!-- <form action=" " method="post">  -->
                                            <div class="modal-header">
                                                <input type="hidden" name="st_id" value="<?php echo $approve['st_id']; ?>">
                                                <h4 class="modal-title" id="exampleModalLabel1">Back To Admission List</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body text-left">
                                                 <h5>Back To Admission List?</h5>
                                            </div>
                                           <div class="modal-footer">
                                            <button type="button" class="btn btn-success" onclick="back_student(<?php echo $approve['st_id']; ?>)">Confirm</button>

                                                <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
                                                 <button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close">Close</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
</span>

                                                               

                                                            </td>
                    </tr>
                   
                 <?php $i++; } ?>
                   
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
    
   
    <!--End footer-->
    
    <!--start color switcher-->
   <!--<div class="right-sidebar">-->
   <!-- <div class="switcher-icon">-->
   <!--   <i class="zmdi zmdi-settings zmdi-hc-spin"></i>-->
   <!-- </div>-->
   <!-- <div class="right-sidebar-content">-->

   <!--   <p class="mb-0">Gaussion Texture</p>-->
   <!--   <hr>-->
      
   <!--   <ul class="switcher">-->
   <!--     <li id="theme1"></li>-->
   <!--     <li id="theme2"></li>-->
   <!--     <li id="theme3"></li>-->
   <!--     <li id="theme4"></li>-->
   <!--     <li id="theme5"></li>-->
   <!--     <li id="theme6"></li>-->
   <!--   </ul>-->

   <!--   <p class="mb-0">Gradient Background</p>-->
   <!--   <hr>-->
      
   <!--   <ul class="switcher">-->
   <!--     <li id="theme7"></li>-->
   <!--     <li id="theme8"></li>-->
   <!--     <li id="theme9"></li>-->
   <!--     <li id="theme10"></li>-->
   <!--     <li id="theme11"></li>-->
   <!--     <li id="theme12"></li>-->
   <!--   </ul>-->
      
   <!--  </div>-->
   <!--</div>-->
  <!--end color cwitcher-->
   
  </div><!--End wrapper-->



  <!-- Chart js -->
  <script src="<?php echo base_url(); ?>assets/plugins/Chart.js/Chart.min.js"></script>
  <!-- Index2 js -->
  <script src="<?php echo base_url(); ?>assets/js/index2.js"></script>



 <!--   Bootstrap core JavaScript-->
  <script src="<?php echo base_url('');?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url('');?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url('');?>assets/js/bootstrap.min.js"></script>
    
  <!-- simplebar js -->
  <script src="<?php echo base_url('');?>assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- sidebar-menu js -->
  <script src="<?php echo base_url('');?>assets/js/sidebar-menu.js"></script>
  
  <!-- Custom scripts -->
  <script src="<?php echo base_url('');?>assets/js/app-script.js"></script> 

  <!--Data Tables js-->
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>

 

    <script>
     $(document).ready(function() {
      //Default data table
       $('#default-datatable').DataTable();


       var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
 
     table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
      
      } );

    </script>

     <script>
     

      function approve_student(st_id) {

       
        $.ajax({
       type: "POST",
       data: {st_id:st_id},
       url: "<?php echo base_url('approve_student') ?>",
       success: function(msg){
      $('#exampleModal1'+st_id+'').modal('hide');
   
      $("#success_message").prepend($('<div class="alert alert-success">SMS Sent Successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>')); 
   location.reload();
   }
});
}

 </script>

   <script>
     

      function back_student(st_id) {

       
        $.ajax({
       type: "POST",
       data: {st_id:st_id},
       url: "<?php echo base_url('back_student') ?>",
       success: function(msg){
      $('#exampleModal1'+st_id+'').modal('hide');
   
      $("#success_message").prepend($('<div class="alert alert-success">SMS Sent Successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>')); 
   location.reload();
   }
});
}

 </script>
    
</body>

<!-- Mirrored from codervent.com/bulona/demo/table-data-tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Jan 2019 10:14:48 GMT -->
</html>

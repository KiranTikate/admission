  <div  id="fee_amount">
    <div class="col-md-3 offset-md-5">
         <button type="button" class="btn waves-effect waves-light btn-rounded btn-dark" onclick="get_all_categories()" id="show_schudle"><i class="fa fa-plus" on aria-hidden="true"></i> Add Fee Amount</button>
   </div><br>
   <div id="fee_amounts"></div>
   <div id="fee_amount1"></div>
   <div class="">
    <!-- order table -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                 <div  id="fee_category_table_success_messsage"></div>
                  <div  id="fee_amount_success_message"></div>
                  <div  id="fee_amount_table_delete_messsage"></div>
                <div id="fee_list"></div>
                <div id="fee_listdelete"></div>
                <!-- Start Table Of Fee Amount -->
                <div class="table-responsive">
                    <table id="fee_amount_table" class="table table-striped table-bordered display" style="width:100%">
                         <thead>
                            <tr>
                                <th>No.</th>
                                <th>Fee Category</th>
                                <th>Sub-Category/ Installement</th>
                               <!-- <th>Sub-Category/  Installment Type</th> -->
                                <th>Class</th>
                                <th>Sub-Category/ Installment Amount</th>
                                <!--<th>Sub-Category/ Installment Discount</th>-->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                           

                           <span class="badge">   
                                        <!-- sample modal content -->
                                      <!--   <button type="button" class="btn waves-effect waves-light btn-sm btn-success" data-toggle="modal" data-target="#exampleModal03" data-whatever="@fat"  data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i>
                                        </button> -->
                                        <!-- Start model -->
                                        <div class="modal fade" id="exampleModal03" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel01">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="exampleModalLabel03">Edit Fee Period</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                     </div>
                                                    <div class="modal-body text-left">
                                                        <form method="post" id="edit_fee_amount_form">
                                                              <input type="hidden" class="form-control" id="fee_category_amount_id" name="fee_category_amount_id">
                                                            <div class="form-group">
                                                                <label for="feecategory"> Fee Category : <span class="danger">*</span> </label><br>
                                                                <select class="custom-select_category form-control" id="kiran_fee_category_id" name="fee_category_id" required >
                                                                    <option value="">Select Fee Category</option>
                                                                   <!--  <option value="India">tution fee</option>
                                                                    <option value="USA">exam fee</option> -->
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="particularname"> Sub-Category/Installement  : <span class="danger">*</span> </label><br>
                                                                 <input type="text" class="form-control" id="kiran_fee_sub_category_name" name="fee_sub_category_name">
                                                              
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="subamount"> Sub-Category Amount : <span class="danger">*</span> </label>
                                                                <input type="text" class="form-control" id="kiran_fee_amount" name="fee_amount">
                                                            </div>
                                                            <!--<div class="form-group">-->
                                                            <!--    <label for="subdiscount"> Sub-Category Discount : <span class="danger">*</span> </label>-->
                                                            <!--    <input type="text" class="form-control" id="fee_discount" name="fee_discount">-->
                                                            <!--</div>-->
                                                            <div class="form-group">
                                                                <label for="sdate" class="control-label">Start Date :</label>
                                                                <input type="date" class="form-control" id="start_date" name="start_date">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="stime">Due Date :</label>
                                                                <input type="date" class="form-control  required" id="due_date" name="due_date">
                                                            </div>
                                                            <button type="submit" class="btn btn-success" id="fee_edit_list"> Submit</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End modal --> 
                                    </span><!-- <span class="badge ">   
                                                                             <div class="modal fade" id="exampleModal104" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel04">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-body">
                                                                    <h3>Are you sure?</h3>
                                                                    <h4>You won't be able to revert this!
                                                                    </h4>
                                                                </div>
                                                                <form method="post" id="delete_ajax_kiran">
                                                                <input type="hidden" class="form-control" id="fee_category_amount_id" value="" name="fee_category_id">   
                                                                <div class="modal-footer">
                                                                   <button type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">No, Cancle</button>
                                                                    <button  type="submit" class="btn waves-effect waves-light btn-success" onclick="delete_ajax_kiran()"  >Yes, Delete it</button>
                                                                    </form>
                                                                </div>
                                                                    </div>
                                                                </div>
                                    </span> -->

                                    <span class="badge ">   
                                    <!-- sample modal content -->
                                   
                                    <div class="modal fade" id="exampleModal124" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel24">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <h3>Are you sure?</h3>
                                                    <h4>You won't be able to revert this!</h4>
                                                </div>
                                                <form id="delete_ajax_kiran" method="post"> 
                                                   <!--  <input type="hidden" name="fee_category_id" id="delete_fee_category_id"> -->
                                                    <input type="hidden" name="fee_category_id" id="delete_fee_category_id_kiran" >
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">No, Cancle
                                                    </button>
                                                    <button  type="submit" class="btn waves-effect waves-light btn-success" >Yes, Delete it
                                                    </button>
                                                </div>
                                            </form>


                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.modal -->
                                </span>
                        </tbody>
                    </table>
                </div>
                <!--End Table Of Fee Amount -->
            </div>
        </div>
    </div> 
</div>
</div>
<div style="display: none;" id="amount_form">
    <div class="col-md-3 offset-md-5">
        <button class="btn waves-effect waves-light btn-rounded btn-dark"  id="show_table_sche"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to Fee Amount List</button>
    </div><br>
    <div id="view"></div>
    <div class="col-lg-12">
        <h6>ADD FEE AMOUNT121</h6>
     
        <form method="post" id="add_fees_kiran">
            <div class="form-body">
                 <div class="card-body" id="add_fees_success_message"></div>
                <div class="card-body">
                    <!-- Start Row -->
                    <div class="row">
                     <div class="col-md-6">
                       <div class="form-group">
                                                                    <label for="particularstat">Fee Category : <span class="danger">*</span> </label>
                                                                  <select class="custom-select_category form-control required" id="marks_category_id" name="fee_category_id">
                                                                    <option value="">Select Fee Category </option>
                                                                  <!--   <option value="India">Late Fees</option>
                                                                    <option value="USA">Tution Fees</option> -->
                                                                    </select> 
                                                                </div>
                    </div>
                    <div class="col-md-6">
                     <div class="form-group">
                        <label for="wlastName3"> Installment Name :<span class="danger">*</span></label>
                        <input type="text" class="form-control" required id="wlastName3" name="fee_sub_category_name"> </div>
                    </div>
                </div>
                <!-- End Row -->
                <!-- Start Row -->
                <div class="row"> 
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="particularamt"> Sub-Category Amount: <span class="danger">*</span></label>
                            <input type="text" class="form-control" required id="particularamt" name="fee_amount"> </div>
                        </div>
                        <div class="col-md-6">
                                 <div class="form-group">
                            <label for="nddate">Select Class :</label>
                            <select class="custom-select form-control required" id="my_fee_collect_class_id" name="id">

                                <option value="">Select Class</option>
                                <?php foreach($class as $class){ ?>
                                <option value="<?php echo $class['id'] ?>"><?php echo $class['class'] ?></option>
                                
                            <?php } ?>
                            </select>
                        </div>
                            </div>
                       
                        </div>
                      
                        <!-- End Row -->
                        <!-- Start Row -->
                       
                        <!-- End row -->
                    </div>
                    <div class="form-actions">
                        <div class="card-body">
                            <div class="form-group mb-0 text-right">
                                <button type="button" class="btn btn-info waves-effect waves-light" onclick="submit_form_kiran();" id="main">Submit</button>  
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

















<!-- 
     <script type="text/javascript">
    $(function(){
    $("#add_fees_amount_form_kiran").submit(function(){

        dataString = $("#add_fees_amount_form_kiran").serialize();
     console.log(dataString);

        // $.ajax({
        //     type: "POST",
        //     url: "<?php echo base_url('delete_fee_category_ajax'); ?>",
        //     data: dataString,
        //     success: function(data){

        //         $("#fee_category_table_delete_messsage").prepend($('<div class="alert alert-success">Fee Category Deleted Successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>')); 
        //         setTimeout(function() { $("#fee_category_table_delete_messsage").hide(); }, 3000);
        //         var mytbl = $("#fee_category_table").DataTable();
        //         mytbl.ajax.reload();
        //          $('#exampleModal24').modal('toggle');
        //     }

        // });

        return false;  //stop the actual form post !important!

    });
});
</script>   -->
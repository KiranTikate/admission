
        <style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #131212;
  text-align: left;
  padding: 8px;
}

/*tr:nth-child(even) {
  background-color: #dddddd;
}*/
</style>


<link href="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">


<?php $this->load->view('lib/header'); ?>

   <!-- start loader -->
   <div id="pageloader-overlay" class="visible incoming"><div class="loader-wrapper-outer"><div class="loader-wrapper-inner"><div class="loader"></div></div></div></div>
   <!-- end loader -->







<!-- Start wrapper-->
 <?php $this->load->view('lib/sidebar'); ?>
  
   <!--End sidebar-wrapper-->









<!--Start topbar header-->

<!--End topbar header-->





<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">

      <!--Start Dashboard Content-->


 


<div class="container" style="background-color: #00c4ffd1;">
    <br>
    <div align="center">
       <b> <span>Student Profile:</span></b>
    </div>

    <div>
     

 <div class="table-responsive">
<table>
 
  <tr>
    <th>NAME</th>
    <td><?php echo $q->fname ; ?></td>
    <th>FATHER NAME</th>
    <td><?php echo $q->father_name; ?></td>
   </tr>
  <tr>
    <th>DOB</th>
    <td><?php echo $q->birth_date; ?></td>
    <th>CONTACT NUMBER</th>
    <td><?php echo $q->ph_no_3; ?></td>
  </tr>
  <tr>
    <th>CLASS</th>
    <td><?php echo $q->class; ?></td>
    <th>EMAIL</th>
    <td><?php echo $q->f_email; ?></td>
  </tr>
 
</table>
</div>
<br>
    </div>
</div>
<br>

<div class="container" style="background-color: #3f9c3fbf;">
    <br>
    <div align="center">
       <b> <span>Fees Structure For <?php echo $q->class; ?> Standard:</span></b>
    </div>

    <div>
     

<!--  <div class="table-responsive">
<table>
  <tr>
    <th>Fee Type</th>
    <th>Amount</th>
    <th>Paying Term</th>
    <th>Installment 1</th>
    <th>Installment 2</th>
  </tr>
  <tr>
    <td>Admission Fees</td>
    <td>8000</td>
    <td>One Time</td>
    <td>-</td>
    <td>-</td>
  </tr>
  <tr>
    <td>Academic Fees</td>
    <td>20000</td>
    <td>Term wise</td>
    <td>10000</td>
    <td>10000</td>
  </tr>
  
  
</table>
</div> -->

 <div class="row">
 <!--   <div class="col-lg-12">
    <div class="card">
       <h6>FEE COLLECT</h6>
       <form action="#">
        <div class="form-body">
            <div class="card-body">
                <div class="row pt-3">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nddate">Select Class :</label>
                            <select class="custom-select form-control required" id="my_fee_collect_id" name="particularN"  onchange="get_fee_collect_data()" >

                                <option value="">Select Class</option>
                               <?php foreach($class as $class){ ?>
                                <option value="<?php echo $class['id'] ?>"><?php echo $class['class'] ?></option>
                                
                            <?php } ?>
                            </select>
                        </div>
                    </div> 


                     

                  
                </div> 
            </div>
           
        </div>
    </form>
</div>
</div> -->
<!-- col-12 -->
<div class="col-12">
   <div class="card">
    <!-- Card body -->
    <div class="" id="fee_category_table_success_messsage">
        <table id="fee_collect_table_data" class="table table-bordered table-responsive-lg table-striped table-bordered">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Class</th>
                    <th>Category</th>
                    <th>Sub-Category</th>
                    <th>Amount</th>
               
                  
                 
                </tr>
            </thead>
            <tbody>
              <?php $i=1; foreach ($payment as $payment)  { ?>
               <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $payment['class'] ?></td>
                <td><?php echo $payment['fee_category_name'] ?></td>
                <td><?php echo $payment['fee_sub_category_name'] ?></td>
                <td><?php echo $payment['fee_amount'] ?></td>

               </tr>
             <?php $i++; } ?>
           </tbody>
       </table>
   </div>
   <!-- Card body -->
</div>
</div>

</div>
<br>
    </div>
</div>



<br>
<div class="container" style="background-color: orange;">
    <br>
    <div align="center">
       <b> <span>To Confirm admission, Please Pay Following admission fees :</span></b>
    </div>

    <div>
<?php if ($status==null) { ?>

<div class="table-responsive">
<table id="example" class="table table-bordered">
   <?php foreach ($payment1 as $payment1)  { ?>

 <tr>
    
    
   <td><?php echo $payment1['fee_sub_category_name'] ?></td>
    <td><?php echo $payment1['fee_amount'] ?></td>
    <td>One Time</td>
    
    <td><a href="<?php echo base_url('user/payment_form/').$payment1['fee_category_amount_id']; ?>"><button type="button" class="btn btn-info btn-lg">Pay Now</button></a></td>


  </tr>

<?php } } else { ?>

 <div class="table-responsive">
<table id="example" class="table table-bordered">
   <?php foreach ($payment1 as $payment1)  { 

       foreach($status as $status ){

if(($payment1['fee_category_amount_id']==$status['fee_id']) AND ($status['txStatus'])=='SUCCESS'){

    ?>
  
  <tr>
    
    
   <td><?php echo $payment1['fee_sub_category_name'] ?></td>
    <td><?php echo $payment1['fee_amount'] ?></td>
    <td>One Time</td>
    
    <td><button type="button" class="btn btn-info btn-lg">PAID</button></td>


  </tr>
<?php } else { ?>
 
  <tr>
    
    
   <td><?php echo $payment1['fee_sub_category_name'] ?></td>
    <td><?php echo $payment1['fee_amount'] ?></td>
    <td>One Time</td>
    
    <td><a href="<?php echo base_url('user/payment_form/').$payment1['fee_category_amount_id']; ?>"><button type="button" class="btn btn-info btn-lg">Pay Now</button></a></td>


  </tr>
     <?php } } } ?>

    
  
  
  
</table>
</div>
<?php } ?>
<br>
    </div>
</div>




<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="modal-title">Payment Details</h2>
      </div>
      <div class="modal-body">
        <h4>HDFC Bank.</h4>
        <p>Branch- Telco Road,Bhosari</p>
        <p>
            A/C Name:  Galaxy School,<br>
            A/C Number: 50200041833710.<br>
            IFSC Codes- HDFC0003841.
           
            
        </p>
        <p>  
            After Payment Call : 090113 43582</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



  
      <!--End Dashboard Content-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
    
   
 
   
  </div><!--End wrapper-->

  <!-- Bootstrap core JavaScript-->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
    
 <!-- simplebar js -->
  <script src="assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- sidebar-menu js -->
  <script src="assets/js/sidebar-menu.js"></script>
  <!-- loader scripts -->
  <script src="assets/js/jquery.loading-indicator.html"></script>
  <!-- Custom scripts -->
  <script src="assets/js/app-script.js"></script>
  <!-- Chart js -->
  
  <script src="assets/plugins/Chart.js/Chart.min.js"></script>
  <!-- Vector map JavaScript -->
  <script src="assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
  <script src="assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- Easy Pie Chart JS -->
  <script src="assets/plugins/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
  <!-- Sparkline JS -->
  <script src="assets/plugins/sparkline-charts/jquery.sparkline.min.js"></script>
  <script src="assets/plugins/jquery-knob/excanvas.js"></script>
  <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
    
 <!--Data Tables js-->
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>

    <script>
     $(document).ready(function() {
      //Default data table
       $('#default-datatable').DataTable();


       var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
 
     table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
      
      } );

    </script>


    <script>
        $(function() {
            $(".knob").knob();
        });
    </script>
  <!-- Index js -->
  <script src="assets/js/index.js"></script>

  
</body>

<!-- Mirrored from codervent.com/bulona/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Jan 2019 09:58:09 GMT -->
</html>

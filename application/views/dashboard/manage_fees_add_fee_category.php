      <div  id="add_subject">
                                        <div class="col-md-3 offset-md-5"> <button type="button" class="btn waves-effect waves-light btn-rounded btn-dark" id="show_manage_fee"><i class="fa fa-plus" aria-hidden="true"></i> Add Fee Category</button>
                                        </div>
                                        <br>
                                        <div id="fee_category"></div>
                                         <div id="delete"></div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card">
                                                <div id="fee_category_table_success_messsage"></div>
                                                <div id="fee_category_table_delete_messsage"></div>
                                                    <div class="" >
                                                <div class="table-responsive">
                                                <table id="fee_category_table" class="table table-striped table-bordered">
                                                <thead>
                                                     <tr>
                                                    <th>No.</th>
                                                    <th>Fee Category</th>
                                                    <th>Description</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    
                                                    
                              
                                <div class="modal fade" id="exampleModal14" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel14">Edit Fee Category</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body text-left">
                                                <form id="edit_fee_category_form" method="post">
                                                    <input type="hidden" name="fee_category_id" id="fee_category_id" value="fee_category_id">
                                                    <div class="form-group">
                                                        <label for="examname" class="control-label" name="fee_name">Fee Category :</label>
                                                        <input type="text" class="form-control" id="fee_category_name" name="fee_category_name" required>
                                                    </div>
                                                   <!--   <div class="form-group">
                                                        <label for="examdate" class="control-label">Invoice Pre Fix :</label>
                                                        <input type="text" class="form-control  required" id="examdatew">
                                                    </div> -->
                                                    <div class="form-group">
                                                        <label for="wlocation2"> Fee Category Status : <span class="danger">*</span> </label><br>
                                                        <select class="custom-select form-control required" id="fee_category_status" name="fee_category_status">
                                                        <option value="">Select Fee Category Status</option>
                                                        <option value="1">Active</option>
                                                        <option value="2">Inactive</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                            <label for="comment">Description :</label>
                                                           <textarea name="fee_category_description" id="fee_category_description" rows="4" class="form-control"></textarea>
                                                    </div>
                                                        <button type="submit" class="btn btn-success" > Submit</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.modal --> 
                                </span><span class="badge ">   
                                    <!-- sample modal content -->
                                   
                                    <div class="modal fade" id="exampleModal24" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel24">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <h3>Are you sure?</h3>
                                                    <h4>You won't be able to revert this!</h4>
                                                </div>
                                                <form id="delete_fee_category_ajax" method="post"> 
                                                   <!--  <input type="hidden" name="fee_category_id" id="delete_fee_category_id"> -->
                                                    <input type="hidden" name="fee_category_id" id="delete_fee_category_id" >
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">No, Cancle
                                                    </button>
                                                    <button  type="submit" class="btn waves-effect waves-light btn-success" >Yes, Delete it
                                                    </button>
                                                </div>
                                            </form>


                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.modal -->
                                </span>
                                
                                </tbody>
                                </table>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div style="display: none;" id="add_exam">
                                    <div class="col-md-3 offset-md-5">
                                        <button class="btn waves-effect waves-light btn-rounded btn-dark" id="show_fee_catagory"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to Fee Category List</button>
                                    </div><br>
                                      <div id="message"></div>
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <h6>ADD FEE CATEGORY1222</h6>
                                                <form id="fee_category_form" method="post">
                                                    <div class="form-body">
                                                        <div id="fee_category_success_message"></div>
                                                        <div class="card-body" >
                                                            <div class="row pt-3">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label">  Fee Category : </label><span class="danger">*</span>
                                                                        <div class="controls">
                                                                            <input type="text" name="fee_category_name" class="form-control" required data-validation-required-message="This field is required"> </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label"> Fee Category Status :</label>
                                                                      <select class="custom-select form-control required" id="wselect" name="fee_category_status">
                                                                    <option value="">Select Category Status</option>
                                                                    <option value="1">Active</option>
                                                                    <option value="2">Inactive</option>
                                                                         </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label"> Note :</label>
                                                                        <div class="controls">
                                                                          <textarea name="fee_category_description" id="note1" rows="4" class="form-control"></textarea></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/row-->
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="card-body">
                                                                <div class="form-group mb-0 text-right">
                                                                    <button type="submit" class="btn btn-info waves-effect waves-light" >Submit</button>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>       
                                    </div>
 
<?php $this->load->view('lib/header'); ?>




<link href="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">

<!-- start loader -->
   <div id="pageloader-overlay" class="visible incoming"><div class="loader-wrapper-outer"><div class="loader-wrapper-inner" ><div class="loader"></div></div></div></div>
   <!-- end loader -->
<?php $this->load->view('lib/sidebar'); ?>

<!--Start topbar header-->


<div class="clearfix"></div>
  
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     

     <div class="row">
     
        <div class="col-lg-10">
           <div class="card">
              <div class="card-body"> 
                  <ul class="nav nav-pills nav-pills-warning" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="pill" href="#piil-4"><i class="icon-home"></i> <span class="hidden-xs">Fee Category</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="pill" href="#piil-5" onclick="get_fee_amount_kiran()"><i class="icon-user"></i> <span class="hidden-xs">Fee Amount</span></a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" data-toggle="pill" href="#piil-6"><i class="icon-settings"></i> <span class="hidden-xs">Class Wise Fees</span></a>
                  </li>
                  
                
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                  <div id="piil-4" class="container tab-pane active">
                     <?php $this->load->view('dashboard/manage_fees_add_fee_category'); ?>
                  </div>
                  <div id="piil-5" class="container tab-pane fade">
                     <?php $this->load->view('dashboard/manage_fees_add_fee_amount'); ?>
                  </div>
                 
                  <div id="piil-6" class="container tab-pane fade">
                    <?php $this->load->view('dashboard/manage_fees_fee_collect'); ?>
                  </div>
                </div>
              </div>
           </div>
        </div>
      </div><!--End Row-->
    <!-- End Breadcrumb-->
    


    
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
  
  <!--Start footer-->

  <!--End footer-->
  

   
  </div><!--End wrapper-->


 

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url('');?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url('');?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url('');?>assets/js/bootstrap.min.js"></script>
  
  <!-- simplebar js -->
  <script src="<?php echo base_url('');?>assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- sidebar-menu js -->
  <script src="<?php echo base_url('');?>assets/js/sidebar-menu.js"></script>
  
  <!-- Custom scripts -->
  <script src="<?php echo base_url('');?>assets/js/app-script.js"></script>

  <!--Data Tables js-->
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>

    <script>
     $(document).ready(function() {
      //Default data table
       $('#default-datatable').DataTable();


       var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel',  ]
      } );
 
     table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
      
      } );

    </script>
  
<script type="text/javascript">
            $('#show_manage_fee').click(function(){
            $('#add_subject').hide();
            $('#add_exam').show();

            }) ;
            $('#show_fee_catagory').click(function(){
            $('#add_subject').show();
            $('#add_exam').hide();

           }) ; /*js for Fee Category Tab*/

            $('#show_schudle').click(function(){
             $('#fee_amount').hide();
             $('#amount_form').show();

           }) ;
            $('#show_table_sche').click(function(){
             $('#fee_amount').show();
             $('#amount_form').hide();

           }) ; /*js for Fee Amount Tab*/
</script>

 <script>
         function(window, document, $) {
            "use strict";
            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        }(window, document, jQuery);

   </script>
   <!-- add fee category -->

    <script>

$(function(){
    $("#fee_category_form").submit(function(){
        dataString = $("#fee_category_form").serialize();

        // alert(dataString); 

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('add_fee_category'); ?>",
            data: dataString,
            success: function(data){
                $("#fee_category_success_message").prepend($('<div class="alert alert-success">Fee Category Added Successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>')); 
                $("#fee_category_form")[0].reset();
                setTimeout(function() { $("#fee_category_success_message").hide(); }, 3000);
                 var mytbl = $("#fee_category_table").DataTable();
                mytbl.ajax.reload();
                
            }

        });

        return false;  //stop the actual form post !important!

    });
});
</script>

<!-- ajax call to append exam data to exam_table -->
<script type="text/javascript">
     var fee_category_table = $('#fee_category_table').DataTable();

function get_category()
{
   
     var base_url="<?php echo base_url("get_fee_category"); ?>";
    

     fee_category_table=$('#fee_category_table').DataTable({
        destroy: true,
        "ajax": base_url,       
        "columns": [
            { "data": "sr_id","width":"1%" },
            { "data": "fee_category_name","width":"1%"  },
            { "data": "fee_category_description","width":"1%" },
            { "data": "fee_category_status_name","width":"1%" }
           
            
            ],
        "columnDefs":   [                               //For Action Buttons (Edit and Delete button) adding in the Action Column
        {      
            "orderable": false,     //Turn off ordering
            "searchable": false,    //Turn off searching
            "targets": 4,          //Going to last column
            
            "data": null,
            "width":"1%",
             
            render : function (data, type, row) {
                return `
                                     <button type="button" class="btn waves-effect waves-light btn-sm btn-danger" id="delete_fee_category_ajax" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i>
                                                                </button>` ;
           }

        }] ,
         
        
    });



}

    $(document).ready(function () {
     
    
        get_category();
     
        fee_category_table.clear()
        .draw();
           
    });



    $('#fee_category_table tbody').on('click', '#edit_fee_category_ajax', function () {
            row_details = fee_category_table.row($(this).parent().parent()).data();
            console.log(row_details['fee_category_id']);
             $('#exampleModal14').modal('show');
             $('#fee_category_id').val(row_details['fee_category_id']);
             $('#fee_category_name').val(row_details['fee_category_name']);
             $('#fee_category_status').val(row_details['fee_category_status']);
             $('#fee_category_description').val(row_details['fee_category_description']);
        });

    $('#fee_category_table tbody').on('click', '#delete_fee_category_ajax', function () {
            row_details = fee_category_table.row($(this).parent().parent()).data();
            console.log(row_details['fee_category_id']);
             $('#exampleModal24').modal('show');
              // $('#fee_category_id').val(row_details['fee_category_id']);
            $('#delete_fee_category_id').val(row_details['fee_category_id']);
             $('#fee_category_amount_id').val(row_details['fee_category_amount_id']);
            
        });
</script>

<!-- edit categories submit -->

<!-- <script type="text/javascript">
    $(function(){
    $("#edit_fee_category_form").submit(function(){

        dataString = $("#edit_fee_category_form").serialize();
        // console.log(dataString);

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('edit_fee_category'); ?>",
            data: dataString,
            success: function(data){

                $("#fee_category_table_success_messsage").prepend($('<div class="alert alert-success">Fee Category Updated Successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>')); 
                setTimeout(function() { $("#fee_category_table_success_messsage").hide(); }, 3000);
                var mytbl = $("#fee_category_table").DataTable();
                mytbl.ajax.reload();
                 $('#exampleModal14').modal('toggle');
            }

        });

        return false;  //stop the actual form post !important!

    });
});
</script> -->


  
 <script type="text/javascript">
    $(function(){
    $("#delete_fee_category_form").submit(function(){

        dataString = $("#delete_fee_category_form").serialize();
     console.log(dataString);

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('delete_fee_category'); ?>",
            data: dataString,
            success: function(data){

                $("#fee_category_table_delete_messsage").prepend($('<div class="alert alert-success">Fee Category Deleted Successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>')); 
                setTimeout(function() { $("#fee_category_table_delete_messsage").hide(); }, 3000);
                var mytbl = $("#fee_category_table").DataTable();
                mytbl.ajax.reload();
                 $('#exampleModal04').modal('toggle');
            }

        });

        return false;  //stop the actual form post !important!

    });
});
</script>  

 <script type="text/javascript">
    $(function(){
    $("#delete_fee_category_ajax").submit(function(){

        dataString = $("#delete_fee_category_ajax").serialize();
     console.log(dataString);

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('delete_fee_category_ajax'); ?>",
            data: dataString,
            success: function(data){

                $("#fee_category_table_delete_messsage").prepend($('<div class="alert alert-success">Fee Category Deleted Successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>')); 
                setTimeout(function() { $("#fee_category_table_delete_messsage").hide(); }, 3000);
                var mytbl = $("#fee_category_table").DataTable();
                mytbl.ajax.reload();
                 $('#exampleModal24').modal('toggle');
            }

        });

        return false;  //stop the actual form post !important!

    });
});
</script>  



<!-- Select Fees Categories -->
<script type="text/javascript">
    
    function onError(xhr,status,error)
     {
      console.log(xhr);
      console.log(status);
      console.log(error);
     }

 function onCategoryDataReceived(data,status,xhr)
     {

          $('.custom-select_category').find('option').remove();
         $('.custom-select_category').append('<option>Select Category</option>');
      if((data.length)!=0)
      {
      $.each(data, function (i, data) {
      $('.custom-select_category').append($('<option>', { 
        value: data.fee_category_id,
        text : data.fee_category_name 
    }));

    });
     }
     else
     {
        $('.custom-select_category').find('option').remove();
         $('.custom-select_category').append('<option>Select Category</option>');
     }      
     }


function get_all_categories()
      {
        // alert('ddd');
        var base_url="<?php echo base_url("get_all_categories"); ?>";

        var request=
       {
         url : base_url,
         type :'GET',
         success : onCategoryDataReceived,
         headers :{ Accept : 'application/json'},
         error : onError
       };

         $.ajax(request);

      }

    $(document).ready(function () {
     
    
        get_category();
     
        table.clear()
        .draw();
           
    });
</script>

<script>

function submit_form_kiran()
{
    // var queryString = $('#add_fees_kiran').formSerialize();
    var queryString = $('#add_fees_kiran').serialize();
// alert(queryString);
console.log(queryString);


  $.ajax({
            type: "POST",
            url: "<?php echo base_url('add_fees_amount_form_kiran'); ?>",
            data: queryString,
            success: function(data){
                $("#add_fees_success_message").prepend($('<div class="alert alert-success">Fees Amount Added Successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>')); 
                setTimeout(function() { $("#add_fees_success_message").hide(); }, 3000);
                $("#add_grade_form")[0].reset();
                  var mytbl = $("#add_fees_table").DataTable();
                mytbl.ajax.reload();
            }

        });
  return false;
}
</script>



<script type="text/javascript">
     var fee_amount_table = $('#fee_amount_table').DataTable();

function get_fee_amount_kiran()
{
   // alert('14');
     var base_url="<?php echo base_url("get_fee_amount"); ?>";
    

     fee_amount_table=$('#fee_amount_table').DataTable({
        destroy: true,
        "ajax": base_url,       
        "columns": [
            { "data": "sr_id","width":"1%" },
            { "data": "fee_category_name","width":"1%"  },
            { "data": "fee_sub_category_name","width":"1%" },
            // { "data": "fee_category_type_name","width":"1%" },
            { "data": "class","width":"1%" },
             { "data": "fee_amount","width":"1%" },
            // { "data": "fee_discount","width":"1%" },
           
            
            ],
        "columnDefs":   [                               //For Action Buttons (Edit and Delete button) adding in the Action Column
        {      
            "orderable": false,     //Turn off ordering
            "searchable": false,    //Turn off searching
            "targets": 5,          //Going to last column
            
            "data": null,
            "width":"1%",
             
            render : function (data, type, row) {
                return `
                                     <button type="button" class="btn waves-effect waves-light btn-sm btn-danger" id="delete_fee_amount_ajax_kiran" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i>
                                                                </button>` ;
           }

        }] ,
         
        
    });



}

    $(document).ready(function () {
     
    
        get_fee_amount_kiran();
     
        fee_amount_table.clear()
        .draw();
           
    });



    $('#fee_amount_table tbody').on('click', '#edit_fee_amount_ajax', function () {
            row_details = fee_amount_table.row($(this).parent().parent()).data();
            // console.log(row_details['comment']);
             $('#exampleModal03').modal('show');
             get_all_categories();

             $('#fee_category_amount_id').val(row_details['fee_category_amount_id']);
             $('#kiran_fee_category_id').val(row_details['fee_category_id']);
             $('#kiran_fee_sub_category_name').val(row_details['fee_sub_category_name']);
             $('#fee_category_type_id').val(row_details['fee_category_type_id']);

              $('#kiran_fee_amount').val(row_details['fee_amount']);
            //  $('#fee_discount').val(row_details['fee_discount']);
             $('#start_date').val(row_details['start_date']);
             $('#due_date').val(row_details['due_date']);
        });

    // $('#fee_amount_table tbody').on('click', '#delete_fee_amount_ajax_kiran', function () {
    //         row_details = fee_amount_table.row($(this).parent().parent()).data();
    //         console.log(row_details['comment']);
    //          $('#exampleModal104').modal('show');
    //       var p=    $('#fee_category_delete_amount_id').val(row_details['fee_category_amount_id']);
    //        alert(p); 
    //     });

       $('#fee_amount_table tbody').on('click', '#delete_fee_amount_ajax_kiran', function () {
            row_details = fee_amount_table.row($(this).parent().parent()).data();
            console.log(row_details['fee_category_id']);
             $('#exampleModal124').modal('show');
              // $('#fee_category_id').val(row_details['fee_category_id']);
            $('#delete_fee_category_id_kiran').val(row_details['fee_category_id']);
             // $('#fee_category_amount_id').val(row_details['fee_category_amount_id']);
            
        });
</script>




  
 <!-- <script type="text/javascript">
    $(function(){
    $("#kiran_delete_fee_amount_ajax_kiran").submit(function(){
      alert('hi');

        dataString = $("#delete_fee_amount_ajax_kiran").serialize();
     console.log(dataString);

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('delete_fee_amount_form'); ?>",
            data: dataString,
            success: function(data){

                $("#fee_amount_table_delete_messsage").prepend($('<div class="alert alert-success">Fee Category Deleted Successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>')); 
                setTimeout(function() { $("#fee_amount_table_delete_messsage").hide(); }, 3000);
                var mytbl = $("#fee_amount_table").DataTable();
                mytbl.ajax.reload();
                 $('#exampleModal04').modal('toggle');
            }
        });

        return false;  //stop the actual form post !important!
    });
});
</script>  
 -->

 <script type="text/javascript">
   
   $(function(){
    $(#"delete_ajax_kiran").submit(function(){
      alert('hi');
    });
   });
 </script>

 <script type="text/javascript">
   function delete_fee_amount_ajax_kiran()
{
  // alert('fii');
    var queryString = $('#fee_category_amount_id').Serialize();
    // var queryString = $('#delete_fee_amount_ajax_kiran').serialize();
// alert(queryString);
console.log(queryString);


  $.ajax({
            // type: "POST",
            // url: "<?php echo base_url('add_fees_amount_form_kiran'); ?>",
            // data: queryString,
            // success: function(data){
            //     $("#add_fees_success_message").prepend($('<div class="alert alert-success">Fees Amount Added Successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>')); 
            //     setTimeout(function() { $("#add_fees_success_message").hide(); }, 3000);
            //     $("#add_grade_form")[0].reset();
            //       var mytbl = $("#add_fees_table").DataTable();
            //     mytbl.ajax.reload();
            // }

        });
  return false;
}
 </script>


<!-- ajax call to append fees collect data to fees_table -->

<!-- ajax call to append fees collect data to fees_table -->
<script type="text/javascript">
     var fee_collect_table_data = $('#fee_collect_table_data').DataTable();

function get_fee_collect_data()
{

  
  
     var base_url="<?php echo base_url("get_fee_collect_data"); ?>";
      var fee_category_amount_id= $("#fee_category_amount_id").val();
     var id= $("#my_fee_collect_id").val();
   
    
    //   alert(division_id);
     table3=$('#fee_collect_table_data').DataTable({
        destroy: true,
        "ajax": base_url+"/"+id,       
        "columns": [
            {"data":"sr_id"},
            {"data":"class"},
            { "data": "fee_category_name" },
            { "data": "fee_sub_category_name" },
            { "data": "fee_amount" },
            // { "data": "fee_discount" },
            // { "data": "total_paid_amount" },
            { "data": "payable_amount" }
            
           
            
            ],
        "columnDefs":   [                               //For Action Buttons (Edit and Delete button) adding in the Action Column
        {      
            "orderable": false,     //Turn off ordering
            "searchable": false,    //Turn off searching
            "targets": 5,          //Going to last column
            
            "data": null,
            
            
            render : function (data, type, row) {

                 return `    
                            <input type="hidden" value="` + row.student_id + `" name='student_id[]'>
                             <input type="hidden" value="` + row.fee_category_amount_id + `" name='fee_category_amount_id[]'>

                   
                                                               ` ;
           }

        }] ,
         
        
    });

// $('#search_result').append('<li class="list_style"><a href="<?php echo base_url('user/profile_friend/') ?>' +'/' +data.id + '">'+uname+'</a></li>');

}
                $('#fee_collect_table_data tbody').on('click', '#pay_fee_collect_ajax', function () {
                    // alert(1);
                    //
            row_details = table3.row($(this).parent().parent()).data();
             // alert(row_details);

            console.log(row_details);
             $('#exampleModal12').modal('show');
             
               $('#fee_category_amount_id_new').val(row_details['fee_category_amount_id']);
                 $('#class_id_new').val(row_details['class_id']);
                   $('#division_id_new').val(row_details['division_id']);
                $('#student_id_new').val(row_details['student_id']);
                  $('#fee_category_id_new').val(row_details['fee_category_id']);


              $('#total_payable_amount').text("Rs. "+row_details['payable_amount']);

            //  $('#due_date_late_charge').val(row_details['due_date_late_charge']);
          
             
        });

    
$(function(){
    $("#pay_fee_collect_form").submit(function(){
        // alert(11);
            var student_id= $("#student_id").val();
            var fee_category_amount_id= $("#fee_category_amount_id_new").val();
           
        dataString = $("#pay_fee_collect_form").serialize();
          console.log(dataString);
          
        $.ajax({
            type: "POST",
            
            url: "<?php echo base_url('pay_fee_collect_form'); ?>"+"/"+student_id+"/"+fee_category_amount_id,
            data: dataString,
             success: function(data){
            // alert(dataString);
                if(data)
          {
              var jsonObj = JSON.parse(data);
              if(jsonObj.status)
              {
                console.log(jsonObj.status);
                console.log('*****');
              }


            console.log(jsonObj.data);
            // window.location.href = "<?php //echo base_url('invoice'); ?>"+"/"+jsonObj.data+"/"+student_id;
            }



              $("#fee_category_table_success_messsage").prepend($('<div class="alert alert-success">Fee Submit Successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>')); 
                setTimeout(function() { $("#fee_category_table_success_messsage").hide(); }, 3000);
                 var mytbl = $("#fee_collect_table_data").DataTable();
                 mytbl.ajax.reload();
                  $('#exampleModal12').modal('toggle');

                     
                    
             }

        });

        return false;  //stop the actual form post !important!

    });
});


</script>

<!--End Display Fees Amount On Table And Edit -->



</body>

<!-- Mirrored from codervent.com/bulona/demo/table-data-tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Jan 2019 10:14:48 GMT -->
</html>

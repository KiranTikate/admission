
<?php $this->load->view('lib/header'); ?>




<link href="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">

<!-- start loader -->
   <div id="pageloader-overlay" class="visible incoming"><div class="loader-wrapper-outer"><div class="loader-wrapper-inner" ><div class="loader"></div></div></div></div>
   <!-- end loader -->
<?php $this->load->view('lib/sidebar'); ?>

<!--Start topbar header-->


<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Data Tables</h4>
		   
	   </div>
	  
     </div>
    <!-- End Breadcrumb-->
    


     <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No.</th>
                         <th> Student Name</th>
                        <th>Class</th>
                        <th>Gender</th>
                        <th>Birth Date</th>
                        <th>Phone Number</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                     <?php $i=1; foreach ($request as $request) { ?>
                  
                    <tr>
                        <td> <?php echo $i; ?></td>
                        <td><?php echo $request['fname'] ?> </td>
                        <td><?php echo $request['class'] ?></td>
                        <td><?php echo $request['gender'] ?></td>
                        <td><?php echo $request['birth_date'] ?></td>
                        <td><?php echo $request['ph_no_3'] ?></td>
                        <td><?php echo $request['created_on'] ?></td>
                        <td>
                            <span class="badge"> 
                            <button type="button" class="btn waves-effect waves-light btn-sm btn-warning" data-toggle="modal" data-target="#exampleModal13<?php echo $request['st_id']; ?>" data-whatever="@fat" data-toggle="tooltip" title="View" ><i class="fa fa-eye" aria-hidden="true"></i>
                                </button>

 <div class="modal fade" id="exampleModal13<?php echo $request['st_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content details">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel13">Student Profile</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body ">
                                              <center><img src="https://hostingspell.com/assets/images/user.png" width="100px" ></center>
                                              <h4 class="text-center"></h4> 
                                              <div class="row text-center">
                                                <div class="col-md-12">
                
                                                     <ul class="list-group" >
   <lable>Student Profile</lable>
     <lable>Student Profile</lable>
    <li class="list-group-item">Child Name :<?php echo $request['fname']; ?> </li>
  
    <li class="list-group-item">Gender : <?php echo $request['gender']; ?></li>
    <li class="list-group-item">Class Of Admission :<?php echo $request['class']; ?></li>
    <li class="list-group-item">Date Of Birth :<?php echo $request['birth_date']; ?> </li>
    <li class="list-group-item">Birth Place:<?php echo $request['birth_place']; ?> </li>
    
   
    
   
    <lable><b>Parent Profile</b></lable>
    <li class="list-group-item">Mother Name:<?php echo $request['mother_name']; ?> </li>
   
    
    <li class="list-group-item">Father Name: <?php echo $request['father_name']; ?> </li>
    <li class="list-group-item">Father Email :<?php echo $request['f_email']; ?></li>
    <li class="list-group-item">Father Occupation: <?php echo $request['f_occup']; ?></li>
        
      <li class="list-group-item">Father Qualification :<?php echo $request['f_quali']; ?> </li>
    <li class="list-group-item">Father Annual Income:<?php echo $request['f_annual_income']; ?> </li>
    <lable><b>COMMUNICATION DETAILS</b></lable>
    <li class="list-group-item">ADDRESS:<?php echo $request['address']; ?> </li>
    <li class="list-group-item">Land Mark : <?php echo $request['land_mark']; ?></li>
    <li class="list-group-item">City :<?php echo $request['city']; ?></li>
    <li class="list-group-item">District :<?php echo $request['district']; ?> </li>
    <li class="list-group-item">Pin Code<?php echo $request['pin_code']; ?> </li>
    
    <li class="list-group-item">State: <?php echo $request['state']; ?> </li>
    <li class="list-group-item">Mobile/Whatsapp Personal No :<?php echo $request['ph_no_3']; ?></li>
    <li class="list-group-item">Parent Number: <?php echo $request['ph_no_4']; ?></li>
   
    <li class="list-group-item">10th School Name :<?php echo $request['school_name']; ?> </li>
    <li class="list-group-item">10th Board<?php echo $request['medium_inst']; ?> </li>
    <li class="list-group-item">12th School Name :<?php echo $request['class_comp']; ?> </li>
    <li class="list-group-item">12th Board<?php echo $request['medium_inst1']; ?> </li>
    
    
       <li class="list-group-item">Mother Tongue Of Student :<?php echo $request['mother_tongue']; ?></li>
    <li class="list-group-item">Admission Fee Paid: <?php echo $request['admission_fee']; ?></li>
     <li class="list-group-item">Reference :<?php echo $request['reference']; ?></li>
    <li class="list-group-item">Select Category: <?php echo $request['student_type']; ?></li>
    
  <li class="list-group-item">Child photo:<a href="<?php echo base_url('upload_images/student_profile_pics/').$request['photo']; ?>" download>  <?php echo $request['photo']; ?></a>

     </li>

     
     
 <li class="list-group-item">Aadhaar Card:<a href="<?php echo base_url('upload_images/student_aadhar/').$request['aadhar_cer']; ?>" download>  <?php echo $request['aadhar_cer']; ?></a>

     </li>
     
     
 <li class="list-group-item">Birth Certificate:<a href="<?php echo base_url('upload_images/student_birth/').$request['birth_cer']; ?>" download>  <?php echo $request['birth_cer']; ?></a>

     </li>
     
     
 <li class="list-group-item">Result of Previous School:<a href="<?php echo base_url('upload_images/student_result/').$request['result_cer']; ?>" download>  <?php echo $request['result_cer']; ?></a>

     </li>
   
</ul>
                                                </div>
                                            
                                              </div>
                                            </div>
                                           <div class="modal-footer">
                                                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                                            </span>

                                                            <span class="badge">
                                                             <button type="button" class="btn waves-effect waves-light btn-sm btn-warning" data-toggle="modal" data-target="#example1<?php echo $request['st_id']; ?>" data-whatever="@fat" data-toggle="tooltip" title="Approve" ><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                </button>

                                  <div class="modal fade" id="example1<?php echo $request['st_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel18">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                             <!-- <form action=" " method="post">  -->
                                            <div class="modal-header">
                                                <input type="hidden" name="st_id" value="<?php echo $request['st_id']; ?>">
                                                <h4 class="modal-title" id="exampleModalLabel1">Approve Admission</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body text-left">
                                                <h5>Approve Admission?</h5>
                                            </div>
                                           <div class="modal-footer">
                                            <button type="button" class="btn btn-success" onclick="approve_student(<?php echo $request['st_id']; ?>)">Confirm</button>

                                                <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
                                                 <button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close">Close</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
</span>

  <span class="badge">
                                                             <button type="button" class="btn waves-effect waves-light btn-sm btn-warning" data-toggle="modal" data-target="#reject1<?php echo $request['st_id']; ?>" data-whatever="@fat" data-toggle="tooltip" title="Reject" ><i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>

                                  <div class="modal fade" id="reject1<?php echo $request['st_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel11">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                             <!-- <form action=" " method="post">  -->
                                            <div class="modal-header">
                                                <input type="hidden" name="st_id" value="<?php echo $request['st_id']; ?>">
                                                <h4 class="modal-title" id="exampleModalLabel1">Reject Admission</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body text-left">
                                                <h5>Reject Admission?</h5>
                                            </div>
                                           <div class="modal-footer">
                                            <button type="button" class="btn btn-success" onclick="delete_student(<?php echo $request['st_id']; ?>)">Confirm</button>

                                                <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
                                                 <button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close">Close</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
</span>

                                                               

                                                            </td>
                    </tr>
                   
                 <?php $i++; } ?>
                   
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--Start footer-->

	<!--End footer-->
	

   
  </div><!--End wrapper-->


  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url('');?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url('');?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url('');?>assets/js/bootstrap.min.js"></script>
	
  <!-- simplebar js -->
  <script src="<?php echo base_url('');?>assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- sidebar-menu js -->
  <script src="<?php echo base_url('');?>assets/js/sidebar-menu.js"></script>
  
  <!-- Custom scripts -->
  <script src="<?php echo base_url('');?>assets/js/app-script.js"></script>

  <!--Data Tables js-->
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>

    <script>
     $(document).ready(function() {
      //Default data table
       $('#default-datatable').DataTable();


       var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
 
     table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
      
      } );

    </script>
	
<script>

 function approve_student(st_id) {

       
        $.ajax({
       type: "POST",
       data: {st_id:st_id},
       url: "<?php echo base_url('approve_student') ?>",
       success: function(msg){
      $('#exampleModal1'+st_id+'').modal('hide');
   // alert(st_id);
      $("#success_message").prepend($('<div class="alert alert-success">SMS Sent Successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>')); 
   location.reload(true);
   }
});
}

 </script>

  <script>
     

      function delete_student(st_id) {

       
        $.ajax({
       type: "POST",
       data: {st_id:st_id},
       url: "<?php echo base_url('delete_student') ?>",
       success: function(msg){
      $('#exampleModal11'+st_id+'').modal('hide');
   
      $("#success_message").prepend($('<div class="alert alert-success">SMS Sent Successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>')); 
      location.reload();
   
   }
});
}

 </script>
    


</body>

<!-- Mirrored from codervent.com/bulona/demo/table-data-tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Jan 2019 10:14:48 GMT -->
</html>


<?php $this->load->view('lib/header'); ?>




<link href="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">

<!-- start loader -->
   <div id="pageloader-overlay" class="visible incoming"><div class="loader-wrapper-outer"><div class="loader-wrapper-inner" ><div class="loader"></div></div></div></div>
   <!-- end loader -->
<?php $this->load->view('lib/sidebar'); ?>

<!--Start topbar header-->


<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     

     <div class="row">
        <div class="col-lg-8">
           <div class="card">
              <div class="card-body"> 
                <ul class="nav nav-pills" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="pill" href="#piil-1"><i class="icon-home"></i> <span class="hidden-xs">Class Listing</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="pill" href="#piil-2"><i class="icon-user"></i> <span class="hidden-xs">Add Class</span></a>
                  </li>
                
                 
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                  <div id="piil-1" class="container tab-pane active">
                    <div align="center">
                    <h5>Class Listing</h5>
                     <div class="table-responsive">
                    <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No.</th>
                        <th>Class</th>
                        <th>Action</th>
                 </thead>
                <tbody>

                    
                  
                    <?php $i=1; foreach ($q as $q) { ?>   
                    <tr>
                      
                        <td><?php echo $i; ?></td>
                        <td><?php echo $q['class']; ?></td>
                       
                        <td> <span class="badge ">   
                                            <!-- sample modal content -->
                                            <button type="button" class="btn waves-effect waves-light btn-sm btn-danger"  data-toggle="modal" data-target="#exampleModal24<?php echo $q['id'];?>" data-whatever="@mdo" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i>
                                            </button>
                                            <div class="modal fade" id="exampleModal24<?php echo $q['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel24">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                          <form action="<?php echo base_url('delete_class/'); ?><?php echo $q['id'];?>" method="post">
                                                        <div class="modal-body">
                                                           
                                                            <h3>Are you sure?</h3>
                                                            <h4>You won't be able to revert this!</h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">No, Cancle</button>
                                                            <button button type="submit" class="btn waves-effect waves-light btn-success" >Yes, Delete it</button>
                                                        </div>
                                                    </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.modal -->
                                            </span> </td>
                    </tr>
                   
                     <?php $i++; } ?>       
                </tbody>
                
            </table>
                  </div></div></div>






                  <div id="piil-2" class="container tab-pane fade">
                    <div align="center">
                    <h5>Add Class </h5>

                    <form method="post" action="<?php echo base_url('add_class') ?>">
                      <input type="text" name="class" placeholder="Class Name">
                      <button type="submit">Add</button>
                    </form>
                  </div>
                 </div>

                 





                  
                </div>
              </div>
           </div>
        </div>

        
      </div><!--End Row-->
    <!-- End Breadcrumb-->
    


    
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--Start footer-->

	<!--End footer-->
	

   
  </div><!--End wrapper-->


 

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url('');?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url('');?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url('');?>assets/js/bootstrap.min.js"></script>
  
  <!-- simplebar js -->
  <script src="<?php echo base_url('');?>assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- sidebar-menu js -->
  <script src="<?php echo base_url('');?>assets/js/sidebar-menu.js"></script>
  
  <!-- Custom scripts -->
  <script src="<?php echo base_url('');?>assets/js/app-script.js"></script>

  <!--Data Tables js-->
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url('');?>assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>

    <script>
     $(document).ready(function() {
      //Default data table
       $('#default-datatable').DataTable();


       var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel',  ]
      } );
 
     table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
      
      } );

    </script>
  

  <script>
     

      function delete_student(st_id) {

       
        $.ajax({
       type: "POST",
       data: {st_id:st_id},
       url: "<?php echo base_url('delete_student') ?>",
       success: function(msg){
      $('#exampleModal11'+st_id+'').modal('hide');
   
      $("#success_message").prepend($('<div class="alert alert-success">SMS Sent Successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>')); 
      location.reload();
   
   }
});
}

 </script>
    


</body>

<!-- Mirrored from codervent.com/bulona/demo/table-data-tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Jan 2019 10:14:48 GMT -->
</html>

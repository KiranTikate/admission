<?php if ($this->agent->platform()=='Android') { ?> 



<!DOCTYPE html>
<html lang="en">
<head>
	<title>Admission Form</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?php echo base_url('') ?>home/img/wklogo.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>form_css_new/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>form_css_new/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>form_css_new/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>form_css_new/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>form_css_new/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>form_css_new/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>form_css_new/css/main.css">
<!--===============================================================================================-->
</head>
<body>

<header>
	<!-- <div style="margin-left: 25px" align="center">
	 <a href="#intro" class="scrollto"><img src="<?php echo base_url('') ?>home/img/wklogo.png" style="width: 75px;" alt="" class="img-fluid"></a>
	 </div> -->
</header>

	<div class="bg-contact100" style="background-image: url('<?php echo base_url('') ?>form_css_new/images/bg-01.jpg');">
		<div class="container-contact100">
		    
			<div class="wrap-contact100">
<span class="hide-it">  
<?php echo $this->session->flashdata('manage_student_msg'); ?> 
</span> 
 
					<div class="row"> 

						<div class="col-xs-12 col-sm-12 col-lg-12">
<div class="row">
							<div class="col-xs-4 col-sm-4 col-lg-4" style="padding-right: 100px;">
							<div align="center">
	 <a href="#intro" class="scrollto"><img src="<?php echo base_url('') ?>home/img/wklogo.png" style="width: 150px" alt="" class="img-fluid"></a>
	 </div>	
								<h5 align="center" style="padding-top: 25px;"><b>ADMISSION PROCESSS</b></h5><br>
								<p style="font-size: 17px;text-align: justify;padding-top: 10px;"><b>A call of Principal-</b>

</p>
<p style="font-size: 17px;text-align: justify;">
Tiranga College of Animation & VFx. Welcomes Students and Parents,

</p>
								<p style="font-size: 17px;text-align: justify;">We possess the wonderful culture of the art, animation, creativity and teaching learning process which develops the aspirant of animation career students and learners.

</p>
<p style="font-size: 17px;text-align: justify;">

We like to invite you to take an initial peek into the heart that beats behind the appealing appearance of Tiranga College of Animation & VFx. We follow open and transparent leadership and maintain professional working team culture. We strongly promote academic achievement among our students and care that our students must develop overall while learning with us. We have built up the vibrant learning culture among students. Tiranga College of Animation & VFx is an innovative college of Arts and animation.
</p>
<p style="font-size: 17px;text-align: justify;">

We have dedicated qualified, experienced and enthusiastic faculty members.</p><br>
								
								
									
		<div class="container">
								    
								   <!--  <div align="center">
								    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Fees Structure</button>
								    </div> -->
								    
								    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="width:160%">
      <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div style="margin-right:250px;">
        <h2 class="modal-title">Payment Details</h2>
      </div></div>
      <div class="modal-body">
     <div class="table-responsive">            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Class</th>
        <th>At the time of Admission</th>
        <th>2-Installment(Before 10th July 2020)</th>
        <th>3-Installment(Before 10th Oct 2020))</th>
        <th>4-Installment(Before 10th Jan 2021)</th>
         <th>4-Installment(Before 10th Jan 2021)</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Nursery</td>
        <td>6100</td>
        <td>4560</td>
        <td>3670</td>
        <td>3670</td>
      </tr>

    </tbody>
  </table>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
  <!--<h2>Fees Structure</h2>-->
  
</div>
														
									<div class="container-contact100-form-btn" style="padding-top: 30px;">
						<a href="https://tirangaeducation.com/admission/userlogin">
						<button class="contact100-form-btn" >
							 Go To Student Login
						</button>
						</a>
					</div>
							</div>


					
							<div class="col-xs-8 col-sm-8 col-lg-8" style="padding-top: 15px;">
								<!-- <div align="center"> -->
								<span  > <h5>
						<u><b>Provisional Admission Form</b></u></h5>
					</span>
				<!-- </div> -->
			 <form method="POST" enctype="multipart/form-data"  action="<?php echo base_url('submit_student'); ?>">
						<div class="container" style="padding-top: 30px">
							<label><b>	Student Profile</b>	</label>
								<div class="row">
									<div class="col-xs 8 col-sm-8 col-lg-8">
											<div class="wrap-input100 validate-input" data-validate = "Name is required">
						<input class="input100" type="text" required name="fname" placeholder="Name Of Student *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-child" aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Gender is required">
					
						 <select class="input100" required id="gender" name="gender" style="border-radius: 25px;height: 43px;padding: 0 30px 0px 50px;width:80%">
                                                        <option value="" selected="">Select Gender</option>
                                                        <option value="Male" >Male</option>
                                                        <option value="Female">Female</option>
                                                    </select>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-puzzle-piece" aria-hidden="true"></i>
						</span>
					</div>
									</div>

								

									
									
								</div>

								<div class="row">
									
										<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Class is required" >
						
						 <select class="input100" required id="class" name="class" style="border-radius: 25px;height: 43px;padding: 0 30px 0px 50px;width:80%;" data-toggle="tooltip" title="Class Of Admission">

                                                        <option value="">Select Class Of Admission</option>
                                                      <?php foreach($class as $class){ ?>
                                                             <option value="<?php echo $class['id']; ?>"><?php echo $class['class']; ?></option>
                                                            <?php } ?>
                                                    </select>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-institution" aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Date is required ">
						<input class="input100" required type="date" name="birth_date" placeholder="Date Of Birth "data-toggle="tooltip" title="Date Of Birth" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-calendar" aria-hidden="true"></i>
						</span>
					</div>
									</div>

								 <div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Class is required">
						<input class="input100" required type="text" name="birth_place" placeholder="Birth Place *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-institution" aria-hidden="true"></i>
						</span>
					</div>
									</div> 
									
								</div>


									<div class="row">
								
									
								</div>
							

								<label><b>Parent Profile</b></label>
								<div class="row">
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = " Name is required">
						<input class="input100" required type="text" name="mother_name" placeholder="Mother Name *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user-circle" aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = " Name is required">
						<input class="input100" required type="text" name="father_name" placeholder="Father Name *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user-o" aria-hidden="true"></i>
						</span>
					</div>
									</div>

								
									
								</div>

								




<div class="row">
									

									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Email">
						<input class="input100" required type="text" name="f_email" placeholder="Email *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-send" aria-hidden="true"></i>
						</span>
					</div>
									</div>

									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100" required type="text" name="mother_tongue" placeholder="Mother Tongue Of Student *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-podcast " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									
									
								</div>

									<div class="row">
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100" required type="text" name="f_occup" placeholder="Father Occupation *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-object-ungroup " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100" required type="text" name="f_quali" placeholder="Father Qualification *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-pencil-square-o " aria-hidden="true"></i>
						</span>
					</div>
									</div>

									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Class is required">
						<input class="input100" required type="text" name="f_annual_income" placeholder="Annual Income *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-phone-square " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									 
								</div>


 <label>COMMUNICATION DETAILS</label>



								<div class="row">
									<div class="col-xs 8 col-sm-8 col-lg-8">
											<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" required type="text" name="address" placeholder="ADDRESS *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-address-book-o  " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100"  type="text" name="land_mark" placeholder="Land Mark" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-university" aria-hidden="true"></i>
						</span>
					</div>
									</div>
								</div> 


								 <div class="row">
									
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100" required type="text" name="city" placeholder="City *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-anchor " aria-hidden="true"></i>
						</span>
					</div>
									</div>

									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Class is required">
						<input class="input100" required type="text" name="district" placeholder="District *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-home " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100" required type="text" name="pin_code" placeholder="Pin Code *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-life-ring " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									
								</div> 

							 	<div class="row">
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100" required type="text" name="state" placeholder="State *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-podcast " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Class is required">
						<input class="input100" required type="text" name="ph_no_3" maxlength="10" minlength="10" placeholder="Mobile/Whatsapp Personal No *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-mobile " aria-hidden="true"></i>
						</span>
					</div>
									</div>

									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Class is required">
						<input class="input100"  type="text" name="ph_no_4" placeholder="Parent Number" maxlength="10" minlength="10" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-mobile " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									
								</div> 


			




				<label>QUALIFICATION</label>
				<div class="row">
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100"  type="text" name="school_name" placeholder="10th School Name *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-podcast " aria-hidden="true"></i>
						</span>
					</div>
									</div>
								<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100"  type="text" name="medium_inst" placeholder="Board *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-podcast " aria-hidden="true"></i>
						</span>
					</div>
									</div>

							
									
								</div>
								<div class="row">
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Class is required">
						<input class="input100"  type="text" name="class_comp" placeholder="12th School Name *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-mobile " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100"  type="text" name="medium_inst1" placeholder="Board *" style="width: 80%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-podcast " aria-hidden="true"></i>
						</span>
					</div>
									</div>

								
							
									
								</div>
<label>Other</label>
								<div class="row">
									
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Gender is required">
					
						 <select class="input100" required id="admission_fee" name="admission_fee" style="border-radius: 25px;height: 43px;padding: 0 30px 0px 50px;width:80%">
                                                        <option value="" selected="">Admission Fee Paid</option>
                                                        <option value="Male" >Yes</option>
                                                        <option value="Female">No</option>
                                                    </select>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-puzzle-piece" aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Gender is required">
					
						 <select class="input100" required id="reference" name="reference" style="border-radius: 25px;height: 43px;padding: 0 30px 0px 50px;width:80%">
                                                        <option value="" selected="">Select Reference</option>
                                                        <option value="Online Source" >Online Source</option>
                                                        <option value="Telephone">Telephone</option>
                                                         <option value="Mr. Sachin Mane" >Mr. Sachin Mane</option>
                                                        <option value="Website">Website</option>
                                                         <option value="Other" >Other</option>
                                                        <option value="Mr. Vijay Borate">Mr. Vijay Borate</option>
                                                         <option value="Walking" >Walking</option>
                                                         <option value="Arshin Bagwan" >Arshin Bagwan</option>
                                                         <option value="Gauri Giri" >Gauri Giri</option>
                                                        <option value="Seminar">Seminar</option>
                                                        <option value="WorkShop">WorkShop</option>
                                                    </select>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-puzzle-piece" aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<!-- <div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Gender is required">
					
						 <select class="input100" required id="student_type" name="student_type" style="border-radius: 25px;height: 43px;padding: 0 30px 0px 50px;width:100%">
                                                        <option value="" selected="">Select Category</option>
                                                        <option value="Existing Student" >Existing Student</option>
                                                        <option value="New Student">New Student</option>
                                                    </select>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-puzzle-piece" aria-hidden="true"></i>
						</span>
					</div>
									</div> -->
										
									
								</div>

						


									<div class="row">
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="custom-file">
												<b>
   				<label class="custom-file-label" for="inputGroupFile01">Student Photo</label></b>
      <input type="file" id="files" name="photo" multiple="multiple" />
    <!--  -->
  </div>
									</div><br>

								<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="custom-file">
												<b>
   				<label class="custom-file-label" for="inputGroupFile01">Birth Certificate</label></b>
      <input type="file" id="files" name="birth_cer" multiple="multiple" />
    <!--  -->
  </div>
									</div><br>
									
								</div>
								<br>
									<div class="row">
									

									<div class="col-xs 6 col-sm-6 col-lg-6">
										<div class="custom-file">
 <b>
    <label class="custom-file-label" for="inputGroupFile01">Aadhaar Card</label></b>
    <input type="file" id="files" name="aadhar_cer" />
  </div>
									</div> <br>
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="custom-file">
												<b>
   				<label class="custom-file-label" for="inputGroupFile01">Result of Previous School</label></b>
      <input type="file" id="files" name="result_cer" multiple="multiple" />
    <!--  -->
  </div>
									</div>
									
								</div>
								<br>
								




									<div class="container-contact100-form-btn" style="padding-right: 90px;">
						<button class="contact100-form-btn">
							Send
						</button>
					</div>
							<!-- </div> -->
						</form>
							</div>




						</div>

</div>

					</div>





				

			</div>
		</div>
	</div>

</div>

<footer style="background-color: #121921;">
	<div class="container" >
		<div class="row" style="color:#ecf5fe;"> 
			<div class="col-xs-12 col-sm-12 col-lg-12">
				<div class="row" style="margin-top: 20px">
					<div class="col-xs-3 col-sm-3 col-lg-3" align="center" style="margin-left: 0px;">
						 <a href="#intro" class="scrollto"><img src="<?php echo base_url('') ?>home/img/wklogo.png" style="width: 95px;" alt="" class="img-fluid"></a><br><br>
						 <p style="font-size: 12px;text-align: justify;line-height: 20px;text-transform:capitalize;color:#ecf5fe;">"People with education, vision and knowledge succeed in the battle of life. It can be the same with your child. An educated child will have greater opportunity to make life's dreams come true. At our school, we realize the full potential of your child, because we believe in complete education. We make each day count.".</p>
					</div>

					<div class="col-xs-5 col-sm-5 col-lg-5" align="center">
						<div class="container">
						 <h5 align="center"><b>Payment Option</b></h5><br>
					<div align="center">
						 <a href="#intro" class="scrollto"><img src="<?php echo base_url('') ?>form_css_new/footer_icons/Small.png" alt="" class="img-fluid" style="height: 154px;"></a></div><br>
						 
						
</div>
					</div>

					<div class="col-xs-4 col-sm-4 col-lg-4">
						<div class="container"style="font-size: 18px;text-align: justify;line-height: 37px;text-transform:capitalize;">
						<h3 ><b>Contact Us</b></h3><br>
						<span style="font-size: 14px">Phone No. : </span>
						<span style="font-size: 14px"><b>(+91) 98906 15191/77208 18111
 </b></span><br>
						<span style="font-size: 14px">Email : </span>
						<span style="font-size: 14px"><b>contact@tirangaeduction.com</b></span>
						
</div>
					</div>
					
				</div>
			</div>
		</div>
		<br>
		 <div class="" align="center" style="color:#ecf5fe;">
      <div class="copyright">
         All Rights Reserved. <a href="http://techwonderkinds.com/" style="color:#ecf5fe;"> Wonderkinds Technologies Pvt. Ltd. © 2020</a>
      </div>
     
    </div>
	</div>
</footer>

<!--===============================================================================================-->
	<script src="<?php echo base_url('') ?>form_css_new/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('') ?>form_css_new/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url('') ?>form_css_new/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('') ?>form_css_new/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('') ?>form_css_new/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>


 <script>
      $(function() {

    $(".hide-it").hide(25000);

});
  </script>
</body>
</html>

<?php } else {?>


<style type="text/css">
	.input200 {
    display: block;
    width: 100%;
    background: #e6e6e6;
    font-family: Montserrat-Bold;
    font-size: 10px;
    line-height: 1.5;
    color: #666666;
}
</style>





<!DOCTYPE html>
<html lang="en">
<head>
	<title>Admission Form</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?php echo base_url('') ?>home/img/wklogo.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>form_css_new/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>form_css_new/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>form_css_new/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>form_css_new/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>form_css_new/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>form_css_new/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>form_css_new/css/main.css">
<!--===============================================================================================-->
</head>
<body>

<header>
	<!-- <div style="margin-left: 25px">
	 <a href="#intro" class="scrollto"><img src="<?php echo base_url('') ?>home/img/wklogo.png" style="width: 163px;" alt="" class="img-fluid"></a>
	 </div> -->
</header>

	<div class="bg-contact100" style="background-image: url('<?php echo base_url('') ?>form_css_new/images/bg-01.jpg');">
		<div class="container-contact100">
		    
			<div class="wrap-contact100">
<span class="hide-it">  
<?php echo $this->session->flashdata('manage_student_msg'); ?> 
</span> 
 
					<div class="row"> 

						<div class="col-xs-12 col-sm-12 col-lg-12">
<div class="row">
							<div class="col-xs-4 col-sm-4 col-lg-4">
							<div align="center">
	 <a href="#intro" class="scrollto"><img src="<?php echo base_url('') ?>home/img/wklogo.png" style="width: 150px" alt="" class="img-fluid"></a>
	 </div>	
								<h5 align="center" style="padding-top: 25px;"><b>ADMISSION PROCESSS</b></h5><br>
								<p style="font-size: 17px;text-align: justify;padding-top: 10px;"><b>A call of Principal-</b>

</p>
<p style="font-size: 17px;text-align: justify;">
Tiranga College of Animation & VFx. Welcomes Students and Parents,

</p>
								<p style="font-size: 17px;text-align: justify;">We possess the wonderful culture of the art, animation, creativity and teaching learning process which develops the aspirant of animation career students and learners.

</p>
<p style="font-size: 17px;text-align: justify;">

We like to invite you to take an initial peek into the heart that beats behind the appealing appearance of Tiranga College of Animation & VFx. We follow open and transparent leadership and maintain professional working team culture. We strongly promote academic achievement among our students and care that our students must develop overall while learning with us. We have built up the vibrant learning culture among students. Tiranga College of Animation & VFx is an innovative college of Arts and animation.
</p>
<p style="font-size: 17px;text-align: justify;">

We have dedicated qualified, experienced and enthusiastic faculty members.</p><br>
								
								
								
								<div class="container">
								    
								  <!--   <div align="center">
								    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Fees Structure</button>
								    </div> -->
								    
								    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="width:160%">
      <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div style="margin-right:250px;">
        <h2 class="modal-title">Payment Details</h2>
      </div></div>
      <div class="modal-body">
                 
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Class</th>
        <th>At the time of Admission</th>
        <th>2-Installment(Before 10th July 2020)</th>
        <th>3-Installment(Before 10th Oct 2020))</th>
        <th>4-Installment(Before 10th Jan 2021)</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Nursery</td>
        <td>6100</td>
        <td>4560</td>
        <td>3670</td>
        <td>3670</td>
      </tr>
   
    </tbody>
  </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
  <!--<h2>Fees Structure</h2>-->
  
</div>
									<div class="container-contact100-form-btn" style="padding-top: 51px;">
						<a href="https://tirangaeducation.com/admission/userlogin">
						<button class="contact100-form-btn" >
							 Go To Student Login
						</button>
						</a>
					</div>
							</div>


					
							<div class="col-xs-8 col-sm-8 col-lg-8">
							
								<span class="contact100-form-title" style="text-align: center;">
						Provisional Admission Form
					</span>
				
					 <form method="POST" enctype="multipart/form-data"  action="<?php echo base_url('submit_student'); ?>">
						<div class="container">
							<label><b>	Student Profile</b>	</label>
								<div class="row">
									<div class="col-xs 8 col-sm-8 col-lg-8">
											<div class="wrap-input100 validate-input" data-validate = "Name is required">
						<input class="input100" type="text" required name="fname" placeholder="Name Of Student *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-child" aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Gender is required">
					
						 <select class="input100" required id="gender" name="gender" style="border-radius: 25px;height: 43px;padding: 0 30px 0px 50px;width:100%">
                                                        <option value="" selected="">Select Gender</option>
                                                        <option value="Male" >Male</option>
                                                        <option value="Female">Female</option>
                                                    </select>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-puzzle-piece" aria-hidden="true"></i>
						</span>
					</div>
									</div>

								

									
									
								</div>

								<div class="row">
									
										<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Class is required" >
						
						 <select class="input100" required id="class" name="class" style="border-radius: 25px;height: 43px;padding: 0 30px 0px 50px;width:100%;" data-toggle="tooltip" title="Class Of Admission">

                                                        <option value="">Select Class Of Admission</option>
                                                      <?php foreach($class as $class){ ?>
                                                             <option value="<?php echo $class['id']; ?>"><?php echo $class['class']; ?></option>
                                                            <?php } ?>
                                                    </select>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-institution" aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Date is required ">
						<input class="input100" required type="date" name="birth_date" placeholder="Date Of Birth "data-toggle="tooltip" title="Date Of Birth" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-calendar" aria-hidden="true"></i>
						</span>
					</div>
									</div>

								 <div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Class is required">
						<input class="input100" required type="text" name="birth_place" placeholder="Birth Place *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-institution" aria-hidden="true"></i>
						</span>
					</div>
									</div> 
									
								</div>


									<div class="row">
								
									
								</div>
							

								<label><b>Parent Profile</b></label>
								<div class="row">
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = " Name is required">
						<input class="input100" required type="text" name="mother_name" placeholder="Mother Name *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user-circle" aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = " Name is required">
						<input class="input100" required type="text" name="father_name" placeholder="Father Name *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user-o" aria-hidden="true"></i>
						</span>
					</div>
									</div>

								
									
								</div>

								




<div class="row">
									

									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Email">
						<input class="input100" required type="text" name="f_email" placeholder="Email * " style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-send" aria-hidden="true"></i>
						</span>
					</div>
									</div>

									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100" required type="text" name="mother_tongue" placeholder="Mother Tongue Of Student *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-podcast " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									
									
								</div>

									<div class="row">
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100" required type="text" name="f_occup" placeholder="Father Occupation *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-object-ungroup " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100" required type="text" name="f_quali" placeholder="Father Qualification *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-pencil-square-o " aria-hidden="true"></i>
						</span>
					</div>
									</div>

									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Class is required">
						<input class="input100" required type="text" name="f_annual_income" placeholder="Annual Income *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-phone-square " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									 
								</div>


 <label>COMMUNICATION DETAILS</label>



								<div class="row">
									<div class="col-xs 8 col-sm-8 col-lg-8">
											<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" required type="text" name="address" placeholder="ADDRESS *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-address-book-o  " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100"  type="text" name="land_mark" placeholder="Land Mark" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-university" aria-hidden="true"></i>
						</span>
					</div>
									</div>
								</div> 


								 <div class="row">
									
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100" required type="text" name="city" placeholder="City *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-anchor " aria-hidden="true"></i>
						</span>
					</div>
									</div>

									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Class is required">
						<input class="input100" required type="text" name="district" placeholder="District *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-home " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100" required type="text" name="pin_code" placeholder="Pin Code *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-life-ring " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									
								</div> 

							 	<div class="row">
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100" required type="text" name="state" placeholder="State *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-podcast " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Class is required">
						<input class="input100" required type="text" name="ph_no_3" maxlength="10" minlength="10" placeholder="Mobile/Whatsapp Personal No *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-mobile " aria-hidden="true"></i>
						</span>
					</div>
									</div>

									<div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Class is required">
						<input class="input100"  type="text" name="ph_no_4" placeholder="Parent Number" maxlength="10" minlength="10" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-mobile " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									
								</div> 


			




				<label>QUALIFICATION</label>
				<div class="row">
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100"  type="text" name="school_name" placeholder="10th School Name *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-podcast " aria-hidden="true"></i>
						</span>
					</div>
									</div>
								<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100"  type="text" name="medium_inst" placeholder="Board *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-podcast " aria-hidden="true"></i>
						</span>
					</div>
									</div>

							
									
								</div>
								<div class="row">
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Class is required">
						<input class="input100"  type="text" name="class_comp" placeholder="12th School Name *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-mobile " aria-hidden="true"></i>
						</span>
					</div>
									</div>
									
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Date is required">
						<input class="input100"  type="text" name="medium_inst1" placeholder="Board *" style="width: 100%">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-podcast " aria-hidden="true"></i>
						</span>
					</div>
									</div>

								
							
									
								</div>
<label>Other</label>
								<div class="row">
									
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Gender is required">
					
						 <select class="input100" required id="admission_fee" name="admission_fee" style="border-radius: 25px;height: 43px;padding: 0 30px 0px 50px;width:100%">
                                                        <option value="" selected="">Admission Fee Paid</option>
                                                        <option value="Male" >Yes</option>
                                                        <option value="Female">No</option>
                                                    </select>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-puzzle-piece" aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="wrap-input100 validate-input" data-validate = "Gender is required">
					
						 <select class="input100" required id="reference" name="reference" style="border-radius: 25px;height: 43px;padding: 0 30px 0px 50px;width:100%">
                                                        <option value="" selected="">Select Reference</option>
                                                        <option value="Online Source" >Online Source</option>
                                                        <option value="Telephone">Telephone</option>
                                                         <option value="Mr. Sachin Mane" >Mr. Sachin Mane</option>
                                                        <option value="Website">Website</option>
                                                         <option value="Other" >Other</option>
                                                        <option value="Mr. Vijay Borate">Mr. Vijay Borate</option>
                                                         <option value="Arshin Bagwan" >Arshin Bagwan</option>
                                                         <option value="Gauri Giri" >Gauri Giri</option>
                                                         <option value="Walking" >Walking</option>
                                                        <option value="Seminar">Seminar</option>
                                                        <option value="WorkShop">WorkShop</option>
                                                    </select>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-puzzle-piece" aria-hidden="true"></i>
						</span>
					</div>
									</div>
									<!-- <div class="col-xs 4 col-sm-4 col-lg-4">
											<div class="wrap-input100 validate-input" data-validate = "Gender is required">
					
						 <select class="input100" required id="student_type" name="student_type" style="border-radius: 25px;height: 43px;padding: 0 30px 0px 50px;width:100%">
                                                        <option value="" selected="">Select Category</option>
                                                        <option value="Existing Student" >Existing Student</option>
                                                        <option value="New Student">New Student</option>
                                                    </select>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-puzzle-piece" aria-hidden="true"></i>
						</span>
					</div>
									</div> -->
										
									
								</div>

						


									<div class="row">
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="custom-file">
												<b>
   				<label class="custom-file-label" for="inputGroupFile01">Student Photo</label></b>
      <input type="file" id="files" name="photo" multiple="multiple" />
    <!--  -->
  </div>
									</div>

								<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="custom-file">
												<b>
   				<label class="custom-file-label" for="inputGroupFile01">Birth Certificate</label></b>
      <input type="file" id="files" name="birth_cer" multiple="multiple" />
    <!--  -->
  </div>
									</div>
									
								</div>
								<br>
									<div class="row">
									

									<div class="col-xs 6 col-sm-6 col-lg-6">
										<div class="custom-file">
 <b>
    <label class="custom-file-label" for="inputGroupFile01">Aadhaar Card</label></b>
    <input type="file" id="files" name="aadhar_cer" />
  </div>
									</div>
									<div class="col-xs 6 col-sm-6 col-lg-6">
											<div class="custom-file">
												<b>
   				<label class="custom-file-label" for="inputGroupFile01">Result of Previous School</label></b>
      <input type="file" id="files" name="result_cer" multiple="multiple" />
    <!--  -->
  </div>
									</div>
									
								</div>
								<br>
								




									<div class="container-contact100-form-btn">
						<button class="contact100-form-btn">
							Send
						</button>
					</div>
							<!-- </div> -->
						</form>
							</div>



						</div>

</div>

					</div>





				

			</div>
		</div>
	</div>

</div>

<footer style="background-color: #121921;">
	<div class="container" >
		<div class="row" style="color:#ecf5fe;"> 
			<div class="col-xs-12 col-sm-12 col-lg-12">
				<div class="row" style="margin-top: 20px">
					<div class="col-xs-3 col-sm-3 col-lg-3" style="margin-left: 0px;">
						 <a href="#intro" class="scrollto"><img src="<?php echo base_url('') ?>home/img/wklogo.png" style="width: 175px;" alt="" class="img-fluid"></a><br><br>
						 <p style="font-size: 12px;text-align: justify;line-height: 20px;text-transform:capitalize;color:#ecf5fe;">"People with education, vision and knowledge succeed in the battle of life. It can be the same with your child. An educated child will have greater opportunity to make life's dreams come true. At our school, we realize the full potential of your child, because we believe in complete education. We make each day count.".</p>
					</div>

					<div class="col-xs-5 col-sm-5 col-lg-5" align="center">
						<div class="container">
						 <h5 align="center"><b>Payment Option</b></h5><br>
					<div align="center">
						 <a href="#intro" class="scrollto"><img src="<?php echo base_url('') ?>form_css_new/footer_icons/Small.png" alt="" class="img-fluid" style="height: 154px;"></a></div><br>
						 
						
</div>
					</div>

					<div class="col-xs-4 col-sm-4 col-lg-4">
						<div class="container"style="font-size: 18px;text-align: justify;line-height: 37px;text-transform:capitalize;">
						<h3 ><b>Contact Us</b></h3><br>
						<span style="font-size: 14px">Phone No. : </span>
						<span style="font-size: 14px"><b>(+91) 98906 15191/77208 18111 </b></span><br>
						<span style="font-size: 14px">Email : </span>
						<span style="font-size: 14px"><b>contact@tirangaeduction.com</b></span>
						
</div>
					</div>
					
				</div>
			</div>
		</div>
		<br>
		 <div class="" align="center" style="color:#ecf5fe;">
      <div class="copyright">
         All Rights Reserved. <a href="http://techwonderkinds.com/" style="color:#ecf5fe;"> Wonderkinds Technologies Pvt. Ltd. © 2020</a>
      </div>
     
    </div>
	</div>
</footer>

<!--===============================================================================================-->
	<script src="<?php echo base_url('') ?>form_css_new/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('') ?>form_css_new/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url('') ?>form_css_new/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('') ?>form_css_new/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('') ?>form_css_new/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>


 <script>
      $(function() {

    $(".hide-it").hide(25000);

});
  </script>
</body>
</html>

<?php } ?>



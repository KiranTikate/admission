
<?php if ($this->agent->platform()=='Android') { ?>   


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Online Admission</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="<?php echo base_url('') ?>home/img/favicon.png" rel="icon">
  <link href="<?php echo base_url('') ?>home/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?php echo base_url('') ?>home/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php echo base_url('') ?>home/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url('') ?>home/lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url('') ?>home/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url('') ?>home/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url('') ?>home/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?php echo base_url('') ?>home/css/style.css" rel="stylesheet">

  
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="#intro" class="scrollto"><img src="<?php echo base_url('') ?>home/img/wklogo.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="#intro">Home</a></li>
          <li><a href="#services">Principal Message</a></li>
          <li><a href="#why-us">Gallery</a></li>
          <li><a href="#about">About Us</a></li>
          <li><a href="#contact">Contact Us</a></li>
        </ul>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  
      
  <section id="intro" class="clearfix">
    
    <div class="container">
        
        <div class="col-lg-6 contactuspagedetails">
 <span class="hide-it">
      <?php echo $this->session->flashdata('manage_student_msg'); ?> </span></div>
      <div class="intro-img">
        <img src="<?php echo base_url('') ?>home/img/intro-img.svg" alt="" class="img-fluid">
      </div>

      <div class="intro-info">
        <h2>Welcome to<br><span> Online Admission</span><br>Wonderkinds</h2>
        <div>
          <h3 style="color: #fcff53">Hurry up..!! <span>Admission</span> Open.</h3>
          <a href="<?php echo base_url('form'); ?>" class="btn-get-started scrollto">Click Hear To Admission</a>
        </div>
      </div>

    </div>
  </section><!-- #intro -->

  <main id="main">

    

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">
        <header>
          <h3 align="center">A P J Abdul Kalam Sir Message</h3>
<div class="row">
  <div class="col-lg-3"><img src="<?php echo base_url('') ?>home/img/testimonial-3.jpg" alt="" class="img-fluid" style="border-radius: 114px;"></div>
  <div class="col-lg-9" style="text-align: justify-all;">
I am delighted to be here at the Indian Institute of Technology Madras and address the members of this renowned place of learning and other guests present here. My greetings to you all. 
<br>Friends, before I begin my address I want to share a thought with all the youth present here. I have met, so far, 11 million youth like you in a decade?s time, in India and abroad. I have seen their hopes, experienced their pains, walked with their aspirations and heard through their despair. All this experience made me learn something about them, which I would like to share with you:
<br>I learnt, every youth wants to be unique, that is, YOU! But the world all around you, is doing its best, day and night, to make you just "everybody else".
Being like everybody else is convenient at the first glance, but not satisfying in the long vision. 
<br>AThe challenge, therefore, my young friends, is that you have to fight the hardest battle, which any human being can ever imagine to fight; and never stop fighting until you arrive at your destined place, that is, a UNIQUE YOU!
<br>Being unique will require excellence, let us understand what is excellence in more detail.
Excellence is a self-imposed self-directed life-long process.
<br>Excellence is not by accident. It is a process, where an individual, organization or nation, continuously strives to better oneself. The performance standards are set by themselves, they work on their dreams with focus and are prepared to take calculated risks and do not get deterred by failures as they move towards their dreams. Then they step up their dreams as they tend to reach the original targets. They strive to work to their potential, in the process, they increase their performance thereby multiplying further their potential and this is an unending life cycle phenomenon. They are not in competition with anyone else, but themselves. That is the culture of excellence. Let me share an important experience from the life of the father of the nation.</div>
  
</div>
  
</header>
        <div class="row">

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-home-outline" style="color: #ff689b; margin-left: 15px;"></i></div>
              <h4 class="title"><a href="">Good Infrastructure</a></h4>
              <p class="description">We are providing better Place and Infrastructure for your child.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-people-outline" style="color: #e9bf06; margin-left: 15px;"></i></div>
              <h4 class="title"><a href="">Experience Teacher</a></h4>
              <p class="description">We providing Experience Teacher for your child's better education.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-paper-outline" style="color: #3fcdc7; margin-left: 15px;"></i></div>
              <h4 class="title"><a href="">Good Curriculum</a></h4>
              <p class="description">We design and change's in school curriculum time-to-time for getting best result.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-body-outline" style="color:#41cf2e; margin-left: 15px;"></i></div>
              <h4 class="title"><a href="">Activity Center</a></h4>
              <p class="description">Various activities including dance, sport, art and crafts, phonics, speech and various others.</p>
            </div>
          </div>

        </div>

      </div>
    </section>
    <!-- #services -->

    <!--==========================
      Why Us Section
    ============================-->
    <section id="why-us" class="wow fadeIn">
      <div class="container">
        <!-- <header class="section-header">
          <h3> </h3>
          <p>.</p>
        </header> -->

        <div class="row row-eq-height justify-content-center">

          <div class="col-lg-6 mb-4">
            <div class="card wow bounceInUp">
                <!-- <i class="fa fa-diamond"></i> -->
              <div class="card-body">
                <img src="<?php echo base_url('') ?>home/img/img00.jpeg" alt="" class="img-fluid" style="border-radius: 114px;">
              </div>
            </div>
          </div>

          <div class="col-lg-6 mb-4">
            <div class="card wow bounceInUp">
                <!-- <i class="fa fa-language"></i> -->
              <div class="card-body">
               <img src="<?php echo base_url('') ?>home/img/img01.jpg" alt="" class="img-fluid" style="border-radius: 114px;">
              </div>
            </div>
          </div>

         <!--  <div class="col-lg-6 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-object-group"></i>
              <div class="card-body">
               
              </div>
            </div>
          </div> -->

        </div>

        <div class="row row-eq-height justify-content-center">

          <div class="col-lg-6 mb-4">
            <div class="card wow bounceInUp">
                <!-- <i class="fa fa-diamond"></i> -->
              <div class="card-body">
                <img src="<?php echo base_url('') ?>home/img/img02.jpg" alt="" class="img-fluid" style="border-radius: 114px;">
              </div>
            </div>
          </div>

          <div class="col-lg-6 mb-4">
            <div class="card wow bounceInUp">
                <!-- <i class="fa fa-language"></i> -->
              <div class="card-body">
                <img src="<?php echo base_url('') ?>home/img/img03.jpg" alt="" class="img-fluid" style="border-radius: 114px;">
              </div>
            </div>
          </div>
<!-- 
          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
            
              <div class="card-body">
                <img src="/img/img05.jpg" alt="" style="width: 100%;">
              </div>
            </div>
          </div> -->

        </div>

      </div>
    </section>

    

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>About Us</h3>
          <p style="text-align: justify-all;">Welcome to Wonderkinds Technologies! We create high quality products that will make your life better..</p>
        <p style="text-align: justify-all;">
WonderKinds is one stop solution providers which help customers leverage their business both strategically and financially. We transform organizations to win in the flat world. WonderKinds believes in "Integrity, Innovation and Serenity". We have a team of highly skilled resources with strong industry exposure and technical expertise which add value at each and every step of software development cycle.</p>
</header>

        <div class="row about-container">

          <div class="col-lg-4 content order-lg-1 order-2">

            

            <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-home"></i></div>
              <h4 class="title"><a href="">Hostel and Day care </a></h4>
              <p>We have Day care & Hostel Facilities for Both Boys & Girls.</p>
            </div>
</div>
 <div class="col-lg-4 content order-lg-1 order-2">
            <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="icon"><i class="fa fa-video-camera"></i></div>
              <h4 class="title"><a href="">CCTV Access To Parents</a></h4>
              <p>We also provide Parents to Access CCTV anytime and anywhere.</p>
            </div>
</div>
 <div class="col-lg-4 content order-lg-1 order-2">
            <div class="icon-box wow fadeInUp" data-wow-delay="0.6s">
              <div class="icon"><i class="fa fa-rocket"></i></div>
              <h4 class="title"><a href="">Mobile App</a></h4>
              <p>We provide school App to Parents to see all daily School Update.</p>
            </div>

          </div>

          <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
            <img src="<?php echo base_url('') ?>home/img/about-img.svg" class="img-fluid" alt="">
          </div>
        </div>

        

      </div>
    </section><!-- #about -->

    <!--==========================
      Clients Section
    ============================-->
    <section id="testimonials" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Inspirational</h3>
        </header>

        <div class="row justify-content-center">
          <div class="col-lg-8">

            <div class="owl-carousel testimonials-carousel wow fadeInUp">
    
              <div class="testimonial-item">
                <img src="<?php echo base_url('') ?>home/img/testimonial-1.jpg" class="testimonial-img" alt="">
                <h3>Dr B R Ambedkar</h3>
                <h4>Jurist, Economist, Politician, Social Reformer</h4>
                <p>
                  Men are mortal. So are ideas. An idea needs propagation as much as a plant needs watering. Otherwise both will wither and die.
                </p>
              </div>
    
              <div class="testimonial-item">
                <img src="<?php echo base_url('') ?>home/img/testimonial-2.jpg" class="testimonial-img" alt="">
                <h3>Mahatma Gandhi</h3>
                <h4>Lawyer, Politician, Activist, Writer</h4>
                <p>
                  True education must correspond to the surrounding circumstances or it is not a healthy growth.
                </p>
              </div>
    
              <div class="testimonial-item">
                <img src="<?php echo base_url('') ?>home/img/testimonial-3.jpg" class="testimonial-img" alt="">
                <h3>Dr. A. P. J. Abdul Kalam</h3>
                <h4>Aerospace scientist, Author</h4>
                <p>
                  Don’t take rest after your first victory because if you fail in second, more lips are waiting to say that your first victory was just luck.
                </p>
              </div>
    
              <div class="testimonial-item">
                <img src="<?php echo base_url('') ?>home/img/testimonial-4.jpg" class="testimonial-img" alt="">
                <h3>Swami Vivekananda</h3>
                <h4>Hindu Monk, Philosopher</h4>
                <p>
                  Arise, awake, and stop not till the goal is reached.
                </p>
              </div>

            </div>

          </div>
        </div>


      </div>
    </section><!-- #testimonials -->

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
      <div class="container-fluid">

        <div class="section-header">
          <h3>Contact Us</h3>
        </div>

        <div class="row wow fadeInUp">

          <div class="col-lg-12">
            <div class="row">
              <div class="col-md-5 info">
                <i class="ion-ios-location-outline"></i>
                <p>Wonderkinds Technology Pvt Ltd, Pune</p>
              </div>
              <div class="col-md-4 info">
                <i class="ion-ios-email-outline"></i>
                <p><a href="mailto:khillcresthighersecondayschool@gmail.com" style="color: #283d50;">info@techwonderkinds.com</a></p>
              </div>
              <div class="col-md-3 info">
                <i class="ion-ios-telephone-outline"></i>
                <p><a href="tel:8974052212" style="color: #283d50;">(+91) 892 814 14 28</a></p>
              </div>
            </div>

            <div class="form">
              <div id="sendmessage">Your message has been sent. Thank you!</div>
              <div id="errormessage"></div>
              <form action="email.php" method="post" role="form" class="contactForm">
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" style="border-radius: 36px;" />
                    <div class="validation"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" style="border-radius: 36px;" />
                    <div class="validation"></div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Your Mobile Number" data-rule="required" data-msg="Please enter a valid mobile number" style="border-radius: 36px;" />
                    <div class="validation"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" style="border-radius: 36px;" />
                    <div class="validation"></div>
                  </div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="4" data-rule="required" data-msg="Please write something for us" placeholder="Message" style="border-radius: 15px;"></textarea>
                  <div class="validation"></div>
                </div>
                <div class="text-center"><button type="submit" title="Send Message">Send Message</button></div>
              </form>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <img src="img/logo.png" alt="" class="img-fluid">
          </div>

          <div class="col-lg-4 col-md-6 footer-contact">
            <h4>Contact Us</h4>
            <p>
              Wonderkinds Technology Pvt Ltd, Pune<br>
              <strong>Phone:</strong> <a href="tel:8974052212" style="color: white;">(+91) 892 814 14 28</a><br>
              <strong>Email:</strong> <a href="mailto:hillcresthighersecondayschool@gmail.comm" style="color: white;">info@techwonderkinds.com</a> <br>
            </p>

          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <div class="social-links">
              <a href="https://" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
              <!-- <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a> -->
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; <strong>Wonderkinds Technology Pvt Ltd, Pune</strong>. All Rights Reserved
      </div>
      <!--<div class="credits">-->
        <!--Powered By <a href="#"> MyEclass</a>-->
      <!--</div>-->
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <div id="preloader"></div>

  <!-- JavaScript Libraries -->
  <script src="<?php echo base_url('') ?>home/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/jquery/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/easing/easing.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/mobile-nav/mobile-nav.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/wow/wow.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/waypoints/waypoints.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/counterup/counterup.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="<?php echo base_url('') ?>home/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="<?php echo base_url('') ?>home/js/main.js"></script>
  <script>
      $(function() {

    $(".hide-it").hide(3000);

});
  </script>
 

</body>
</html>


<?php } else {  ?>




<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Online Admission</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="<?php echo base_url('') ?>home/img/favicon.png" rel="icon">
  <link href="<?php echo base_url('') ?>home/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?php echo base_url('') ?>home/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php echo base_url('') ?>home/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url('') ?>home/lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url('') ?>home/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url('') ?>home/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url('') ?>home/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?php echo base_url('') ?>home/css/style.css" rel="stylesheet">

  
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="#intro" class="scrollto"><img src="<?php echo base_url('') ?>home/img/wklogo.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="#intro">Home</a></li>
          <li><a href="#services">Principal Message</a></li>
          <li><a href="#why-us">Gallery</a></li>
          <li><a href="#about">About Us</a></li>
          <li><a href="#contact">Contact Us</a></li>
        </ul>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  
      
  <section id="intro" class="clearfix">
    
    <div class="container">
        
        <div class="col-lg-6 contactuspagedetails">
 <span class="hide-it">
      <?php echo $this->session->flashdata('manage_student_msg'); ?> </span></div>
      <div class="intro-img">
        <img src="<?php echo base_url('') ?>home/img/intro-img.svg" alt="" class="img-fluid">
      </div>

      <div class="intro-info">
        <h2>Welcome to<br><span> Online Admission</span><br>Wonderkinds</h2>
        <div>
          <h3 style="color: #fcff53">Hurry up..!! <span>Admission</span> Open.</h3>
          <a href="<?php echo base_url('form'); ?>" class="btn-get-started scrollto">Click Hear To Admission</a>
        </div>
      </div>

    </div>
  </section><!-- #intro -->

  <main id="main">

    

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">
        <header>
          <h3 align="center">A P J Abdul Kalam Sir Message</h3>
<div class="row">
  <div class="col-lg-3"><img src="<?php echo base_url('') ?>home/img/testimonial-3.jpg" alt="" class="img-fluid" style="border-radius: 114px;"></div>
  <div class="col-lg-9" style="text-align: justify-all;">
I am delighted to be here at the Indian Institute of Technology Madras and address the members of this renowned place of learning and other guests present here. My greetings to you all. 
<br>Friends, before I begin my address I want to share a thought with all the youth present here. I have met, so far, 11 million youth like you in a decade?s time, in India and abroad. I have seen their hopes, experienced their pains, walked with their aspirations and heard through their despair. All this experience made me learn something about them, which I would like to share with you:
<br>I learnt, every youth wants to be unique, that is, YOU! But the world all around you, is doing its best, day and night, to make you just "everybody else".
Being like everybody else is convenient at the first glance, but not satisfying in the long vision. 
<br>AThe challenge, therefore, my young friends, is that you have to fight the hardest battle, which any human being can ever imagine to fight; and never stop fighting until you arrive at your destined place, that is, a UNIQUE YOU!
<br>Being unique will require excellence, let us understand what is excellence in more detail.
Excellence is a self-imposed self-directed life-long process.
<br>Excellence is not by accident. It is a process, where an individual, organization or nation, continuously strives to better oneself. The performance standards are set by themselves, they work on their dreams with focus and are prepared to take calculated risks and do not get deterred by failures as they move towards their dreams. Then they step up their dreams as they tend to reach the original targets. They strive to work to their potential, in the process, they increase their performance thereby multiplying further their potential and this is an unending life cycle phenomenon. They are not in competition with anyone else, but themselves. That is the culture of excellence. Let me share an important experience from the life of the father of the nation.</div>
  
</div>
  
</header>
        <div class="row">

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-home-outline" style="color: #ff689b; margin-left: 15px;"></i></div>
              <h4 class="title"><a href="">Good Infrastructure</a></h4>
              <p class="description">We are providing better Place and Infrastructure for your child.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-people-outline" style="color: #e9bf06; margin-left: 15px;"></i></div>
              <h4 class="title"><a href="">Experience Teacher</a></h4>
              <p class="description">We providing Experience Teacher for your child's better education.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-paper-outline" style="color: #3fcdc7; margin-left: 15px;"></i></div>
              <h4 class="title"><a href="">Good Curriculum</a></h4>
              <p class="description">We design and change's in school curriculum time-to-time for getting best result.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-body-outline" style="color:#41cf2e; margin-left: 15px;"></i></div>
              <h4 class="title"><a href="">Activity Center</a></h4>
              <p class="description">Various activities including dance, sport, art and crafts, phonics, speech and various others.</p>
            </div>
          </div>

        </div>

      </div>
    </section>
    <!-- #services -->

    <!--==========================
      Why Us Section
    ============================-->
    <section id="why-us" class="wow fadeIn">
      <div class="container">
        <!-- <header class="section-header">
          <h3> </h3>
          <p>.</p>
        </header> -->

        <div class="row row-eq-height justify-content-center">

          <div class="col-lg-6 mb-4">
            <div class="card wow bounceInUp">
                <!-- <i class="fa fa-diamond"></i> -->
              <div class="card-body">
                <img src="<?php echo base_url('') ?>home/img/img00.jpeg" alt="" class="img-fluid" style="border-radius: 114px;">
              </div>
            </div>
          </div>

          <div class="col-lg-6 mb-4">
            <div class="card wow bounceInUp">
                <!-- <i class="fa fa-language"></i> -->
              <div class="card-body">
               <img src="<?php echo base_url('') ?>home/img/img01.jpg" alt="" class="img-fluid" style="border-radius: 114px;">
              </div>
            </div>
          </div>

         <!--  <div class="col-lg-6 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-object-group"></i>
              <div class="card-body">
               
              </div>
            </div>
          </div> -->

        </div>

        <div class="row row-eq-height justify-content-center">

          <div class="col-lg-6 mb-4">
            <div class="card wow bounceInUp">
                <!-- <i class="fa fa-diamond"></i> -->
              <div class="card-body">
                <img src="<?php echo base_url('') ?>home/img/img02.jpg" alt="" class="img-fluid" style="border-radius: 114px;">
              </div>
            </div>
          </div>

          <div class="col-lg-6 mb-4">
            <div class="card wow bounceInUp">
                <!-- <i class="fa fa-language"></i> -->
              <div class="card-body">
                <img src="<?php echo base_url('') ?>home/img/img03.jpg" alt="" class="img-fluid" style="border-radius: 114px;">
              </div>
            </div>
          </div>
<!-- 
          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
            
              <div class="card-body">
                <img src="/img/img05.jpg" alt="" style="width: 100%;">
              </div>
            </div>
          </div> -->

        </div>

      </div>
    </section>

    

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>About Us</h3>
          <p style="text-align: justify-all;">Welcome to Wonderkinds Technologies! We create high quality products that will make your life better..</p>
        <p style="text-align: justify-all;">
WonderKinds is one stop solution providers which help customers leverage their business both strategically and financially. We transform organizations to win in the flat world. WonderKinds believes in "Integrity, Innovation and Serenity". We have a team of highly skilled resources with strong industry exposure and technical expertise which add value at each and every step of software development cycle.</p>
</header>

        <div class="row about-container">

          <div class="col-lg-4 content order-lg-1 order-2">

            

            <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-home"></i></div>
              <h4 class="title"><a href="">Hostel and Day care </a></h4>
              <p>We have Day care & Hostel Facilities for Both Boys & Girls.</p>
            </div>
</div>
 <div class="col-lg-4 content order-lg-1 order-2">
            <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="icon"><i class="fa fa-video-camera"></i></div>
              <h4 class="title"><a href="">CCTV Access To Parents</a></h4>
              <p>We also provide Parents to Access CCTV anytime and anywhere.</p>
            </div>
</div>
 <div class="col-lg-4 content order-lg-1 order-2">
            <div class="icon-box wow fadeInUp" data-wow-delay="0.6s">
              <div class="icon"><i class="fa fa-rocket"></i></div>
              <h4 class="title"><a href="">Mobile App</a></h4>
              <p>We provide school App to Parents to see all daily School Update.</p>
            </div>

          </div>

          <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
            <img src="<?php echo base_url('') ?>home/img/about-img.svg" class="img-fluid" alt="">
          </div>
        </div>

        

      </div>
    </section><!-- #about -->

    <!--==========================
      Clients Section
    ============================-->
    <section id="testimonials" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Inspirational</h3>
        </header>

        <div class="row justify-content-center">
          <div class="col-lg-8">

            <div class="owl-carousel testimonials-carousel wow fadeInUp">
    
              <div class="testimonial-item">
                <img src="<?php echo base_url('') ?>home/img/testimonial-1.jpg" class="testimonial-img" alt="">
                <h3>Dr B R Ambedkar</h3>
                <h4>Jurist, Economist, Politician, Social Reformer</h4>
                <p>
                  Men are mortal. So are ideas. An idea needs propagation as much as a plant needs watering. Otherwise both will wither and die.
                </p>
              </div>
    
              <div class="testimonial-item">
                <img src="<?php echo base_url('') ?>home/img/testimonial-2.jpg" class="testimonial-img" alt="">
                <h3>Mahatma Gandhi</h3>
                <h4>Lawyer, Politician, Activist, Writer</h4>
                <p>
                  True education must correspond to the surrounding circumstances or it is not a healthy growth.
                </p>
              </div>
    
              <div class="testimonial-item">
                <img src="<?php echo base_url('') ?>home/img/testimonial-3.jpg" class="testimonial-img" alt="">
                <h3>Dr. A. P. J. Abdul Kalam</h3>
                <h4>Aerospace scientist, Author</h4>
                <p>
                  Don’t take rest after your first victory because if you fail in second, more lips are waiting to say that your first victory was just luck.
                </p>
              </div>
    
              <div class="testimonial-item">
                <img src="<?php echo base_url('') ?>home/img/testimonial-4.jpg" class="testimonial-img" alt="">
                <h3>Swami Vivekananda</h3>
                <h4>Hindu Monk, Philosopher</h4>
                <p>
                  Arise, awake, and stop not till the goal is reached.
                </p>
              </div>

            </div>

          </div>
        </div>


      </div>
    </section><!-- #testimonials -->

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
      <div class="container-fluid">

        <div class="section-header">
          <h3>Contact Us</h3>
        </div>

        <div class="row wow fadeInUp">

          <div class="col-lg-12">
            <div class="row">
              <div class="col-md-5 info">
                <i class="ion-ios-location-outline"></i>
                <p>Wonderkinds Technology Pvt Ltd, Pune</p>
              </div>
              <div class="col-md-4 info">
                <i class="ion-ios-email-outline"></i>
                <p><a href="mailto:khillcresthighersecondayschool@gmail.com" style="color: #283d50;">info@techwonderkinds.com</a></p>
              </div>
              <div class="col-md-3 info">
                <i class="ion-ios-telephone-outline"></i>
                <p><a href="tel:8974052212" style="color: #283d50;">(+91) 892 814 14 28</a></p>
              </div>
            </div>

            <div class="form">
              <div id="sendmessage">Your message has been sent. Thank you!</div>
              <div id="errormessage"></div>
              <form action="email.php" method="post" role="form" class="contactForm">
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" style="border-radius: 36px;" />
                    <div class="validation"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" style="border-radius: 36px;" />
                    <div class="validation"></div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Your Mobile Number" data-rule="required" data-msg="Please enter a valid mobile number" style="border-radius: 36px;" />
                    <div class="validation"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" style="border-radius: 36px;" />
                    <div class="validation"></div>
                  </div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="4" data-rule="required" data-msg="Please write something for us" placeholder="Message" style="border-radius: 15px;"></textarea>
                  <div class="validation"></div>
                </div>
                <div class="text-center"><button type="submit" title="Send Message">Send Message</button></div>
              </form>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <img src="img/logo.png" alt="" class="img-fluid">
          </div>

          <div class="col-lg-4 col-md-6 footer-contact">
            <h4>Contact Us</h4>
            <p>
              Wonderkinds Technology Pvt Ltd, Pune<br>
              <strong>Phone:</strong> <a href="tel:8974052212" style="color: white;">(+91) 892 814 14 28</a><br>
              <strong>Email:</strong> <a href="mailto:hillcresthighersecondayschool@gmail.comm" style="color: white;">info@techwonderkinds.com</a> <br>
            </p>

          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <div class="social-links">
              <a href="https://" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
              <!-- <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a> -->
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; <strong>Wonderkinds Technology Pvt Ltd, Pune</strong>. All Rights Reserved
      </div>
      <!--<div class="credits">-->
        <!--Powered By <a href="#"> MyEclass</a>-->
      <!--</div>-->
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <div id="preloader"></div>

  <!-- JavaScript Libraries -->
  <script src="<?php echo base_url('') ?>home/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/jquery/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/easing/easing.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/mobile-nav/mobile-nav.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/wow/wow.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/waypoints/waypoints.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/counterup/counterup.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url('') ?>home/lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="<?php echo base_url('') ?>home/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="<?php echo base_url('') ?>home/js/main.js"></script>
  <script>
      $(function() {

    $(".hide-it").hide(3000);

});
  </script>
 

</body>
</html>

<?php } ?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
if ( ! function_exists('pretty_print')){
  //success alert
  function pretty_print($data)
  { 
    echo "<pre>";print_r($data);echo "</pre>";
    return $data;
  }
}
// if ( ! function_exists('success_alert')){
  //success alert
  function success_alert($msg_text)
  { //echo "success_alert";
      //$data = array(
      $result="<div class='alert alert-success alert-dismissible fadeIn ' role='alert'>
                      <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                      </button>
                      <strong>Success!</strong> $msg_text
                  </div>";
      //          );
         return $result;
  }
// }
if ( ! function_exists('failure_alert')){
  function failure_alert($msg_text)
  { //echo "failure_alert";
    //$data = array(
    $result="<div class='alert alert-danger alert-dismissible fadeIn' role='alert'>
    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
        <span aria-hidden='true'>×</span>
    </button>
    <strong>Error!</strong> $msg_text.
    </div>";
    //);
    return $result;
  }
}

if(!function_exists('emailConfig'))
{
    function emailConfig()
    {
        $CI->load->library('email');
        $config['protocol'] = PROTOCOL;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['mailpath'] = MAIL_PATH;
        $config['charset'] = 'UTF-8';
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;
    }
}



//verify password
if (! function_exists('verify_password')) {
  function verify_password($password,$hashed_password)
  {
    // If the password inputs matched the hashed password in the database
    // Do something, you know... log them in
    if($password==$hashed_password)
    {
      return 1;
    }
    else
    {
      return ' ';
    }
    // return password_verify($password,$hashed_password);
  }
}

// to get data by id
if (!function_exists('getRecordOnId'))
{
    function getRecordOnId($table, $where){
        $CI =& get_instance();
        $CI->db->from($table);
        $CI->db->where($where);
        $query = $CI->db->get();
        return $query->row();
    }
}

// how to use:$recordUser = getRecordOnId('users', ['id' => 5]); //here 5 is user Id which we can get from session or URL.

// end to get data by id


// image upload
   function upload_image($folder_name,$file_name,$table_name,$data_array,$redirect_to)
  {


         $config['upload_path']   = $folder_name; 
         $config['allowed_types'] = 'gif|jpg|png'; 
         $this->upload->initialize($config);
  
       if ( ! $this->upload->do_upload($file_name)) {
            $error = array('error' => $this->upload->display_errors()); 
            $message =' 
         <div id= "danger-alert" class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
        Image file is not compatible!
       </div>';
             $this->session->set_flashdata('message',$message);
       redirect($redirect_to,'refresh');
         }
      
         else { 
             $data =  $this->upload->data(); 
                       
              $file_name=$data['file_name'];

              $q=$this->Basic_Function_Model->upload_image($file_name,$table_name,$data_array);

              if($q==1)
              {
                $message =' 

         <div id= "success-alert" class="alert alert-success alert-dismissible">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
          Registered Successfully!</div>';
             $this->session->set_flashdata('message',$message);
       redirect($redirect_to,'refresh');
              }

              else
              {
                  $message =' 
         <div id= "danger-alert" class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
        Image file is not compatible!
       </div>';
             $this->session->set_flashdata('message',$message);
       redirect($redirect_to,'refresh');
              }

             
         } 


       
   } 
// end image upload

// File upload start
    function upload_file($folder_name,$file_name,$table_name,$data_array,$redirect_to)
  {


         $config['upload_path']   = $folder_name; 
         $config['allowed_types'] = 'doc|pdf|txt'; 
         $this->upload->initialize($config);
  
       if ( ! $this->upload->do_upload($file_name)) {
            $error = array('error' => $this->upload->display_errors()); 
            $message =' 
         <div id= "danger-alert" class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
        Image file is not compatible!
       </div>';
             $this->session->set_flashdata('message',$message);
       redirect($redirect_to,'refresh');
         }
      
         else { 
             $data =  $this->upload->data(); 
                       
              $file_name=$data['file_name'];

              $q=$this->Basic_Function_Model->upload_image($file_name,$table_name,$data_array);

              if($q==1)
              {
                $message =' 

         <div id= "success-alert" class="alert alert-success alert-dismissible">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
          Registered Successfully!</div>';
             $this->session->set_flashdata('message',$message);
       redirect($redirect_to,'refresh');
              }

              else
              {
                  $message =' 
         <div id= "danger-alert" class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
        Image file is not compatible!
       </div>';
             $this->session->set_flashdata('message',$message);
       redirect($redirect_to,'refresh');
              }

             
         } 


       
   } 
// File upload end 

//Email starts

   function send_email($email_from,$email_to,$subject,$message)
   {
     // Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
    $headers .= 'From: ' .$email_from. "\r\n";
    mail($email_to,$subject,$message,$headers);
   }

//Email Ends

   // sms starts
//    function send_sms($message)
//    {
//     $req = ""; 
// $param['msg'] = $message;
// $param['user'] = "SPDSTR";
// $param['password'] = "SPDSTR";
// $param['msisdn'] = $contact_no;
// $param['sid'] = "DNYPRB";
// $param['fl'] = 0;
// $param['gwid'] = 2;
// foreach ($param as $key => $val) {
// $req.= $key . "=" . urlencode($val);
// $req.= "&";
// }
// $req = substr($req, 0, strlen($req) - 1);
// $url = "http://sms.vndsms.com/vendorsms/pushsms.aspx"? . $req;
// $ch = curl_init($url);
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// $data = curl_exec($ch);
// curl_close($ch);
//    }

   // sms ends


<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class User_login extends CI_Controller
	{
		public function __construct()
		{
				parent::__construct();
				 $this->load->model('Login_model');
				 $this->load->model('Welcome_Model');
				  $this->load->model('User_login_model');
		}

		public function index()
	{
		// $data['user_type']=$this->admin_model->get_user_type();
		// $data['system_data']=$this->admin_model->get_system_setting_data();
		
		$this->load->view('user_login');
	}

	

		public function user_sign_in()
	 	{

	 			// print_r("expression");exit();
			$query = $this->User_login_model->sign_in();
			  	// $user_id1=$this->session->userdata('user_id');
			  	// print_r($query);exit();
            // $count= $this->admin_model->mail_notification_count();
			  // print_r($count);exit();
			 // $this->session->set_userdata('count',$count);
			  // $mail= $this->admin_model->mail_notification();
			  // $this->session->set_userdata('mail',$mail);

			 //$gallery= $this->admin_model->gallery_notification_count();
			 // 	$this->session->set_userdata('gallery',$gallery);


			 //$homework= $this->admin_model->homework_notification_count(); 
				// $this->session->set_userdata('homework',$homework);

			 //$staff= $this->admin_model->staff_notification_count();
			 //	$this->session->set_userdata('staff',$staff);

			 //  $mark= $this->admin_model->mark_notification_count();
			 //  	$this->session->set_userdata('mark',$mark);

			 //  $attendence= $this->admin_model->attendence_notification_count();
			 //  	$this->session->set_userdata('attendence',$attendence);

			 //  	$teacher=$gallery+$staff;
			 //  	$parent=$gallery+$mark+$attendence+$homework;

			 //  	 	$this->session->set_userdata('teacher',$teacher);

			 //  	 	 	$this->session->set_userdata('parent',$parent);
			   	 	 	
			//    	 	 		$academic_year_id=$this->admin_model->academic_year_id();
			// $this->session->set_userdata('academic_year_id',$academic_year_id);
// 	print_r("*********");		   	 	 	
// print_r($gallery);print_r("*********");
// print_r($mark);print_r("*********");
// print_r($attendence);print_r("*********");
// print_r($homework);		print_r("*********");	   	 	 	
// print_r($parent);exit();
            
			// $system_data=$this->admin_model->get_system_setting_data();

			// $this->session->set_userdata('school_footer',$system_data->school_footer);
			// $this->session->set_userdata('school_logo',$system_data->school_logo);

			// print_r($query);exit;

			
			if($query==1)
			{
				// echo "hii";exit;
				$email = $this->input->post('email');
				$data= array('temp_email' => $email );
				$this->load->view('reset_password',$data);
			}
			else if($query==2) // if the user's credentials validated...
			{
			//add_user_activity('Login','Login Success');
			
// 			print_r($this->session->userdata('user_type_id'));exit;
				$msg= 'Sign in successfully!';
				$message=success_alert($msg);

				 $this->session->set_flashdata('message',$message);

				 if($this->session->userdata('user_type_id') == 2)
				 {
				 		redirect("user");
				 }

				 // else if($this->session->userdata('user_type_id') == 4)
				 // {
				 // 	redirect("teacher");
				 // }
				 // else if($this->session->userdata('user_type_id') == 5 || $this->session->userdata('user_type_id') == 6)
				 // {
				     
				 // 	redirect("MyParent");
				 // }
				 // else
				 // {
				 // 	redirect('login');
				 // }
				
				// redirect(base_url(),'refresh');
				
			}
			else  // incorrect username or password
			{
				$msg="Invalid Username or Password";
				$message=failure_alert($msg);
				 $this->session->set_flashdata('message',$message);
				redirect('userlogin');
				
			}
	}


	//  forgot password verification 
		 public function check_forgot_password()
     {

		$q=$this->login_model->check_forgot_password();
		// echo $q;exit;

	// 	if($q==1 )
	// 	{
			

	// 	    $msg = ' New password and reset password link has been sent to your email!.';
 //            $message=success_alert($msg);
	// 		 $this->session->set_flashdata('message',$message);
	// 		         redirect(base_url(),'refresh');
	// 	}

	// else if($q==2)
	// 	{
			
	// 	  $msg = ' New password and reset password link has been sent to your Phone Number!.';
 //            $message=success_alert($msg);
	// 		 $this->session->set_flashdata('message',$message);
	// 		         redirect(base_url(),'refresh');
	// 	}
	// else
	// 	{
	// 		$msg=' Invalid Username!';
	// 		 $message=failure_alert($msg);
		   
 //                $this->session->set_flashdata('message',$message);
	// 		        redirect(base_url(),'refresh');

	// 	}
		
     //}
	// end forgot password verification

	
	     
	}

	// reset password login
	public function reset_password_login()
	{
		if($this->session->userdata('user_type_id'))
		{
			
  		 
			$this->load->view('dashboard/admin/dashboard');
				 
				
		}
		else
		{
			$data['user_type']=$this->admin_model->get_user_type();
			$this->load->view('reset_password_login');
		}

		
	}

	// end reset password login

	// reset password submit

	public function reset_password_submit()
    {	
    	$uname=$this->input->post('username');
    	$newPassword = password_hash($this->input->post('password'),PASSWORD_DEFAULT);
    	$usersData = array('password'=>$newPassword, 'password_reset_flag'=>0); 

        $result = $this->login_model->reset_password($uname, $usersData);

               if($result) { 
                	$msg= 'Password Updated Successfully!Now Please Login with updated Password.';
			        $message=success_alert($msg);

				 $this->session->set_flashdata('message',$message);
			        redirect('login','refresh');
                }
                else { 
                    $msg="Please Enter A Valid Password";
			       	$message=failure_alert($msg);
				 $this->session->set_flashdata('message',$message);
			       
			        redirect('login','refresh');
                } 
     }

	// end reset password submit

     public function change_password_submit()
     {
     	 $q=$this->login_model->change_password_submit();

    if($q == 1)
    {
      $msg="Password Changed Successfully";
			       	$message=success_alert($msg);
				 $this->session->set_flashdata('message',$message);
         redirect('change_password','refresh');
    }
    else
    {

                  $msg="Error in Changing Password";
			       	$message=failure_alert($msg);
				 $this->session->set_flashdata('message',$message);

        redirect('change_password','refresh');
    }
     }
}

?>

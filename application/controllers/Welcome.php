<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Welcome_Model');
       $this->load->library('user_agent');

    }

	public function index()
	{
	      $data['class']=$this->Welcome_Model->class_list();
		$this->load->view('form',$data);
	}

	public function form(){
     $data['class']=$this->Welcome_Model->class_list();
    //  print_r($data);exit();
		$this->load->view('form',$data);
	}
public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url(),'refresh');
	}
	public function submit_student(){
		

 $ip = $_SERVER['REMOTE_ADDR'];

 

// $photo_upload=$_FILES["photo"]["name"];
// $document_upload=$_FILES["document"]["name"];



//  if($_FILES["photo"]["name"]!=null){
//  $config['upload_path']   = 'upload_images/student_profile_pics'; 
//          $config['allowed_types'] = 'gif|jpg|png'; 
//          $this->upload->initialize($config);

//           if ( ! $this->upload->do_upload('photo')) {
//             $error = array('error' => $this->upload->display_errors()); 
//             $msg="Image File is not Compatible!";
//                  $message=failure_alert($msg);
//              $this->session->set_flashdata('manage_student_msg',$message);
     
//          }
//          else{
//               $image_data =  $this->upload->data(); 
//               $photo=$image_data['file_name'];
//              }
//  }
// else{
//   $photo=null;
//   // print_r($photo);
 
// }

if($_FILES["photo"]["name"]!=null){
  $config['upload_path']   = 'upload_images/student_profile_pics'; 
         $config['allowed_types'] = 'gif|jpg|png|pfd|doc'; 
         $this->upload->initialize($config);

          if ( ! $this->upload->do_upload('photo')) {
            $error = array('error' => $this->upload->display_errors()); 
            $msg="Document File is not Compatible!";
                 $message=failure_alert($msg);
             $this->session->set_flashdata('manage_student_msg',$message);
       // redirect('manage_student','refresh');
         }
         else{
              $image_data =  $this->upload->data(); 
               $photo=$image_data['file_name'];
               // print_r($document);
         }
}else{
  $photo=null;
  // print_r($document);
}

if($_FILES["result_cer"]["name"]!=null){
  $config['upload_path']   = 'upload_images/student_result'; 
         $config['allowed_types'] = 'gif|jpg|png|pfd|doc'; 
         $this->upload->initialize($config);

          if ( ! $this->upload->do_upload('result_cer')) {
            $error = array('error' => $this->upload->display_errors()); 
            $msg="Document File is not Compatible!";
                 $message=failure_alert($msg);
             $this->session->set_flashdata('manage_student_msg',$message);
       // redirect('manage_student','refresh');
         }
         else{
              $image_data =  $this->upload->data(); 
               $result_cer=$image_data['file_name'];
               // print_r($document);
         }
}else{
  $result_cer=null;
  // print_r($document);
}

if($_FILES["aadhar_cer"]["name"]!=null){
  $config['upload_path']   = 'upload_images/student_aadhar'; 
         $config['allowed_types'] = 'gif|jpg|png|pfd|doc'; 
         $this->upload->initialize($config);

          if ( ! $this->upload->do_upload('aadhar_cer')) {
            $error = array('error' => $this->upload->display_errors()); 
            $msg="Document File is not Compatible!";
                 $message=failure_alert($msg);
             $this->session->set_flashdata('manage_student_msg',$message);
       // redirect('manage_student','refresh');
         }
         else{
              $image_data =  $this->upload->data(); 
               $aadhar_cer=$image_data['file_name'];
               // print_r($document);
         }
}else{
  $aadhar_cer=null;
  // print_r($document);
}

if($_FILES["birth_cer"]["name"]!=null){
  $config['upload_path']   = 'upload_images/student_birth'; 
         $config['allowed_types'] = 'gif|jpg|png|pfd|doc'; 
         $this->upload->initialize($config);

          if ( ! $this->upload->do_upload('birth_cer')) {
            $error = array('error' => $this->upload->display_errors()); 
            $msg="Document File is not Compatible!";
                 $message=failure_alert($msg);
             $this->session->set_flashdata('manage_student_msg',$message);
       // redirect('manage_student','refresh');
         }
         else{
              $image_data =  $this->upload->data(); 
               $birth_cer=$image_data['file_name'];
               // print_r($document);
         }
}else{
  $birth_cer=null;
  // print_r($document);
}


    $p=$this->input->post();

  
   // exit();
           $student_data=[ 
            "fname"=>$this->input->post('fname'),
          
            "gender"=>$this->input->post('gender'),
            "birth_date"=>$this->input->post('birth_date'),
            "birth_place"=>$this->input->post('birth_place'),
          
            "mother_name"=>$this->input->post('mother_name'),
            
            "father_name"=>$this->input->post('father_name'),
            "f_email"=>$this->input->post('f_email'),
            "f_occup"=>$this->input->post('f_occup'),
            "f_quali"=>$this->input->post('f_quali'),
             "f_annual_income"=>$this->input->post('f_annual_income'),
            "address"=>$this->input->post('address'),
             "land_mark"=>$this->input->post('land_mark'),
            "city"=>$this->input->post('city'),
            "district"=>$this->input->post('district'),
            "pin_code"=>$this->input->post('pin_code'),
            "state"=>$this->input->post('state'),
            "ph_no_3"=>$this->input->post('ph_no_3'),
            "ph_no_4"=>$this->input->post('ph_no_4'),
          
            "school_name"=>$this->input->post('school_name'),
             "class_comp"=>$this->input->post('class_comp'),
         
            "medium_inst"=>$this->input->post('medium_inst'),
             "medium_inst1"=>$this->input->post('medium_inst1'),
           
            "mother_tongue"=>$this->input->post('mother_tongue'),
           "admission_fee"=>$this->input->post('admission_fee'),
            "reference"=>$this->input->post('reference'),
            "student_type"=>$this->input->post('student_type'),
            "class"=>$this->input->post('class'),
            "photo"=>$photo,
            // "document"=>$document,
            "birth_cer"=>$birth_cer,
            "aadhar_cer"=>$aadhar_cer,
            "result_cer"=>$result_cer
            ];
//             
              // exit();
   $q=$this->Welcome_Model->submit_student($student_data);
   
   
//         {
       
//           echo $i;
//             // $phone=  $ph_no;
//           $phone="7276895000";
//             //  $password= $pass;

//   $req = "";  
//          $param['msg'] =  "New Admission Enquiry Recived Go to Admin and Check It.  ";
//          $param['user'] = "SPDSTR";
//          $param['password'] = "SPDSTR";
//          $param['msisdn'] = $phone;
//          $param['sid'] = "SCHOOL";
//          $param['fl'] = 0;
//          $param['gwid'] = 2;
//          foreach ($param as $key => $val) {
//              $req.= $key . "=" . urlencode($val);
//              $req.= "&";
//          }
//          $req = substr($req, 0, strlen($req) - 1);
//          $url = "http://sms.vndsms.com/vendorsms/pushsms.aspx?" . $req;
//          $ch = curl_init($url);
//          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//          $data = curl_exec($ch);
//          curl_close($ch);


// // end sms code
        
//     }
   // exit();



      if($q)
    {
         $q=$this->Welcome_Model->student_class();
         
        // $sql="SELECT * FROM class WHERE id=$this->input->post('class')";
        
        $this->load->library('email');
       
        // $email_from='kirantikate4171@gmail.com';
        $email_from=$this->input->post('f_email');

     $to ='tikaterh@gmail.com,tirangaeducation@gmail.com';
    //  $to ='kirantikate@rediffmail.com,';
        // $to ='tikatehr@gmail.com';
     $fname=$this->input->post('fname');
  
     $gender=$this->input->post('gender');
    $birth_date=$this->input->post('birth_date');
      $class1=$q->class;
    
        $ph_no_4=$this->input->post('ph_no_4');
        $ph_no_3=$this->input->post('ph_no_3');
       
         $city=$this->input->post('city');
    
    

    $subject = 'New Admission Enquiry';
   $mymessage = "
         <html>
     

         <title>Admission Enquiry-</title>
         </head>
         <body>
         <h3>Admission Enquiry</h3>
         <p>Resp Sir,</p>
         <p> New Admission Enquiry <b>recived</b> </p>
    
     <p>Details of admission enquery,</p>
        
  <p><b> Student Name: </b> :".$fname."</p>

      <p><b> Gender: </b> :".$gender."</p>
      <p><b> Birth Date: </b> :".$birth_date."</p>
        <p><b> Class of Admission: </b> :".$class1."</p>
 
         <p><b>Phone Number1 :  </b> :".$ph_no_3."</p>
          <p><b>Phone Number2 :  </b> :".$ph_no_4."</p>
           <p><b>City :  </b> :".$city."</p>

        
      <p><b>Thank You</b></p>


      
         </body>
         </html>
         ";






   // Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
    $headers .= 'From: ' .$email_from. "\r\n";
    mail($to,$subject,$mymessage,$headers);
    
    
//     $this->session->set_flashdata('manage_student_msg', 'Student Admissionn Form Submit Successfully!');
//     $this->load->view('form');
  
} 
    
    if($q)
              {
                //  $msg="Thanks for submitting details,Our College administrative will contact you soon!";
                //  $message=success_alert($msg);
                //  $this->session->set_flashdata('manage_student_msg',$message);
               
                //   redirect('Welcome/form');
                
                 $this->load->view('thankyou');
              }
            else
              {
                // $msg="Error in Submission!";
                // $message=failure_alert($msg);
                // $this->session->set_flashdata('manage_student_msg',$message);
                //  redirect('Welcome/form');
                
                 $this->load->view('thankyou_1');
              }





// print_r($photo);
// echo('******');
// print_r($document);

	}







 






















}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class User extends CI_Controller
{
  public function __construct()
    {
        parent::__construct();
         $this->load->model('Login_model');
         $this->load->model('Welcome_Model');
          $this->load->model('User_login_model');
          $this->load->library('user_agent');

    }

 public function index(){
    
$name=$this->session->userdata(); 
   
     // print_r($name);exit();
$user_id=$name['st_id'];
$class=$name['class']; 
$data['q']=$this->Welcome_Model->user_details($user_id);
$data['payment']=$this->Welcome_Model->fees_structure($class);
$data['payment1']=$this->Welcome_Model->fees_structure1($class);
$data['status']=$this->Welcome_Model->fees_status($user_id);
    // print_r($data);exit();
    $this->load->view('dashboard/user/dashboard',$data);
  }


public function payment(){
   
   $name=$this->session->userdata(); 
   
     // print_r($name);exit();
$user_id=$name['st_id'];

   // print_r($data);exit();
 $secretkey = "6ec6e36e9f8a51416561e14dfc2bfd43434d7ba1";
     $orderId = $_POST["orderId"];
     $orderAmount = $_POST["orderAmount"];
     $referenceId = $_POST["referenceId"];
     $txStatus = $_POST["txStatus"];
     $paymentMode = $_POST["paymentMode"];
     $txMsg = $_POST["txMsg"];
     $txTime = $_POST["txTime"];
     // $orderNote = $_POST["orderNote"];
     $signature = $_POST["signature"];
     $data = $orderId.$orderAmount.$referenceId.$txStatus.$paymentMode.$txMsg.$txTime;
     $hash_hmac = hash_hmac('sha256', $data, $secretkey, true) ;
     $computedSignature = base64_encode($hash_hmac);
     

    


   $paymentData= Array(

     "orderId" => $_POST["orderId"],
     "orderAmount" => $_POST["orderAmount"],
     "referenceId" => $_POST["referenceId"],
     "txStatus" => $_POST["txStatus"],
     "paymentMode" => $_POST["paymentMode"],
     "txMsg" => $_POST["txMsg"],
     "txTime" => $_POST["txTime"],
     "signature" => $_POST["signature"],
     // "orderNote" =>$_POST["orderNote"],
     "customerId" =>$user_id,

     );

// print_r($paymentData);exit();
    
      $q=$this->db->insert('payment',$paymentData);
  $this->load->view('dashboard/user/payment');
}

public function payment_form($fee_category_amount_id){



      $name=$this->session->userdata(); 
$user_id=$name['st_id'];
$class=$name['class'];


$data['q']=$this->Welcome_Model->user_details($user_id);
$data['payment']=$this->Welcome_Model->fees_payment($class,$fee_category_amount_id);

//  print_r($data);exit();
$this->load->view('dashboard/user/payment_form',$data);


// print_r($data);
}


public function submit_payment(){
 
//   print_r($this->input->post()); exit();
    
    $pay_status=$this->Welcome_Model->payment_status();

//  print_r($pay_status);
// exit();

$p=extract($_POST);
//  print_r($p);
// exit();

 $postData = array( 
  "appId" => $appId, 
  "orderId" => $orderId, 
  "orderAmount" => $orderAmount, 
  "returnUrl" => $returnUrl, 
  "notifyUrl" => $notifyUrl,
  // "orderNote" => $orderNote,    
  "customerName" => $customerName, 
  "customerPhone" => $customerPhone, 
  "customerEmail" => $customerEmail,
 
);

$secretKey = "6ec6e36e9f8a51416561e14dfc2bfd43434d7ba1";

ksort($postData);
$signatureData = "";
foreach ($postData as $key => $value){
    $signatureData .= $key.$value;
}
$signature = hash_hmac('sha256', $signatureData, $secretKey,true);
$signature = base64_encode($signature);

$postData1['p'] = array( 
  "appId" => $appId, 
  "orderId" => $orderId, 
  "orderAmount" => $orderAmount, 
  "returnUrl" => $returnUrl, 
  "notifyUrl" => $notifyUrl,    
  "customerName" => $customerName, 
  "customerPhone" => $customerPhone, 
  "customerEmail" => $customerEmail,
  // "orderNote" => $orderNote,
 "signature" =>$signature,
);

$data['q']=$postData1;

// print_r($postData1);exit();

$this->load->view('dashboard/user/final_pay',$data);

}

}

?>